# AutoRTS

AutoRTS is a regression test selection (RTS) system implementing the Extends
Implements New Static (EINS) analysis.

AutoRTS is built with Gradle. The repository includes scripts to install and run
Gradle (requires internet connection). On a Linux/Mac machine, type the following:

    ./gradlew


On a windows machine, run `gradlew.bat` instead.

If the build completed successfully Gradle will write `BUILD SUCCESSFUL` on the
console, you should should then find a new file named `launcher.jar` in the
subdirectory `launcher/build/libs`. The Jar is runnable with Java 6-8.

AutoRTS is run by running the `launcher.jar` file and supplying some command-line options.
Here is a synopsis of the available commands:

* `analyze` - analyze dependencies for a project.
* `runtests` - run the tests for a project, using test selection.
* `compile` - compile (with javac) the system under test.
* `runall` - run all tests (no RTS).

A project is specified by giving the `-project <path>` option.

## Cloning

This project must be built with a specific version of ExtendJ to work properly.

To get the right version of ExtendJ, which is included in the repository as a Git submodule,
use the following command:

    git clone --recursive https://bitbucket.org/joqvist/autorts


Note the `--recursive` flag, which tells Git to also download submodules. If you forgot
the `--recursive` flag, you can run these commands to download the right version of ExtendJ:

    git submodule init
    git submodule update


The ExtendJ code has evolved since this project was actively developed, and some things
that are used in AutoRTS are no longer in ExtendJ, or have changed names.
