package launcher;

import org.jastadd.testsel.PersistentConfiguration;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLClassLoader;
import java.security.CodeSource;
import java.util.ArrayList;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

/**
 * Launcher for the Dependency Viewer application.
 *
 * Unpacks libraries from the Jar file lib directory.
 *
 * @author Jesper Öqvist <jesper.oqvist@cs.lth.se>
 */
public class AutoTesterLauncher {

	private static final boolean DEBUG = false;

	/**
	 * Entry point for the Dependency Viewer application
	 * @param args Command line arguments
	 */
	public static void main(String[] args) {
		try {
			ClassLoader parentCL = AutoTesterLauncher.class.getClassLoader();

			// build list of library jar files
			CodeSource src = AutoTesterLauncher.class
					.getProtectionDomain().getCodeSource();
			List<URL> jars = new ArrayList<URL>();

			if (src != null) {
				URL jar = src.getLocation();
				ZipInputStream in = new ZipInputStream(jar.openStream());
				ZipEntry entry = null;

				while ( (entry = in.getNextEntry()) != null ) {
					String name = entry.getName();
					if (name.endsWith(".jar")) {
						if (name.startsWith("lib")) {
							jars.add(unpackJar(parentCL, name));
						} else if (name.startsWith("tools")) {
							unpackJar(parentCL, name);
						}
					}
				}
			}

			URL[] urls = new URL[jars.size()];
			if (DEBUG) {
				System.out.print("classpath: ");
			}
			for (int i = 0; i < jars.size(); ++i) {
				urls[i] = jars.get(i);
				if (DEBUG) {
					if (i > 0) {
						System.out.print(File.pathSeparator);
					}
					System.out.print(urls[i]);
				}
			}
			if (DEBUG) {
				System.out.println();
			}
			URLClassLoader childCL = new URLClassLoader(urls, parentCL);

			Class<?> mainClass = childCL.loadClass("org.jastadd.autotest.AutoTester");
			Object instance = mainClass.newInstance();
			Method runMethod = mainClass.getDeclaredMethod("run", String[].class);
			runMethod.invoke(instance, new Object[] { args });
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (SecurityException e) {
			e.printStackTrace();
		} catch (InstantiationException e) {
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		} catch (IllegalArgumentException e) {
			e.printStackTrace();
		} catch (MalformedURLException e) {
			e.printStackTrace();
		} catch (NoSuchMethodException e) {
			e.printStackTrace();
		} catch (InvocationTargetException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Unpack the jar file to a temporary directory. Idea is from JDotSoft's JarClassLoader.
	 * @param parentCL parent class loader
	 * @param name library name
	 * @return unpacked JAR ref
	 * @throws IOException
	 */
	private static URL unpackJar(ClassLoader parentCL, String name)
			throws IOException {

		if (DEBUG) {
			System.out.println("Unpacking library: " + name);
		}

		File settingsDir = PersistentConfiguration.settingsDirectory();
		File subdir = new File(settingsDir, getSubDir(name));
		if (!subdir.isDirectory()) {
			if (!subdir.mkdirs()) {
				throw new IOException("Failed to set up library directory: " + subdir.getPath());
			}
		}

		File libFile = new File(settingsDir, name);
		libFile.setReadable(true, false);
		libFile.setWritable(true, false);
		libFile.setExecutable(true, false);
		BufferedOutputStream out = new BufferedOutputStream(new FileOutputStream(libFile));
		InputStream in = parentCL.getResourceAsStream(name);
		byte[] buffer = new byte[4096];
		int len;
		while ((len = in.read(buffer)) != -1) {
			out.write(buffer, 0, len);
		}
		out.close();
		return libFile.toURI().toURL();
	}

	private static String getSubDir(String name) {
		if (name.startsWith("lib")) {
			return "lib";
		} else {
			return "tools";
		}
	}

}
