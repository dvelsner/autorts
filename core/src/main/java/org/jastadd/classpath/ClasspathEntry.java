package org.jastadd.classpath;

import java.io.File;
import java.util.Collection;
import java.util.Collections;
import java.util.LinkedList;

import org.jastadd.testsel.project.Project;
import org.jastadd.testsel.util.ProgramProperties;

/**
 * A classpath entry in the .classpath file
 *
 * @author Jesper Öqvist <jesper.oqvist@cs.lth.se>
 */
public class ClasspathEntry {

	public final String kind;
	public final String path;

	public ClasspathEntry(String kind, String path) {
		this.kind = kind;
		this.path = path;
	}

	public boolean isSourcePath() {
		return kind.equals("src");
	}

	public boolean isLibrary() {
		return kind.equals("lib");
	}

	public boolean isContainer() {
		return kind.equals("con") && !getContainerFiles().isEmpty();
	}

	public File getFile(Project project) {
		if (isSourcePath() || isLibrary()) {
			return project.getLocalFile(path);
		}
		// TODO return container files?
		return new File("unknown");
	}

	public Collection<File> getContainerFiles() {
		if (kind.equals("con")) {
			if (path.equals("org.eclipse.jdt.junit.JUNIT_CONTAINER/4")) {
				Collection<File> files = new LinkedList<File>();
				File junit = ProgramProperties.getJarFile("tools/junit-4.11.jar");
				if (junit != null) {
					files.add(junit.getAbsoluteFile());
				}
				File hamcrest = ProgramProperties
						.getJarFile("tools/hamcrest-core-1.3.jar");
				if (hamcrest != null) {
					files.add(hamcrest.getAbsoluteFile());
				}
				return files;
			} else if (path.equals("org.eclipse.jdt.junit.JUNIT_CONTAINER/3.5")) {
				File junit = ProgramProperties.getJarFile("tools/junit-3.5.jar");
				if (junit != null) {
					return Collections.singleton(junit);
				}
			} else if (path.equals("org.eclipse.jdt.junit.JUNIT_CONTAINER/3.8")) {
				File junit = ProgramProperties.getJarFile("tools/junit-3.8.2.jar");
				if (junit != null) {
					return Collections.singleton(junit);
				}
			}
		}
		return Collections.emptySet();
	}

	@Override
	public String toString() {
		if (isContainer()) {
			if (path.equals("org.eclipse.jdt.junit.JUNIT_CONTAINER/3")) {
				return "JUnit 3";
			} else if (path.equals(
					"org.eclipse.jdt.junit.JUNIT_CONTAINER/4")) {
				return "JUnit 4";
			}
		}
		return path;
	}

}
