package org.jastadd.autotest;

import java.io.File;
import java.lang.reflect.InvocationTargetException;
import java.util.Collections;
import java.util.Iterator;
import java.util.Set;

import org.jastadd.jastaddj.JastAddJVersion;
import org.jastadd.log.Log;
import org.jastadd.testsel.ConsoleProgressListener;
import org.jastadd.testsel.DependencyUpdater;
import org.jastadd.testsel.TestSelector;
import org.jastadd.testsel.meta.DependencyNode;
import org.jastadd.testsel.meta.SourceFile;
import org.jastadd.testsel.project.Project;
import org.jastadd.testsel.testapi.ConsoleTestListener;
import org.jastadd.testsel.util.ProgramProperties;

/**
 * Auto tester main class. If this is started outside IDE,
 * the entry point should be the AutoTesterLauncher class, which calls this.
 *
 * @author Jesper Öqvist <jesper.oqvist@cs.lth.se>
 */
public class AutoTester extends TestSelector {

  private DependencyUpdater updater;

  public AutoTester() {
    super(Project.DEFAULT_PROJECT);
  }

  public static void main(String[] args) {
    AutoTester main = new AutoTester();
    main.run(args);
  }

  enum Command {
    NONE,
    COMPILE,
    ANALYZE,
    RUNTESTS,
    RUNALL,
  }

  /**
   * Run the dependency viewer.
   * @param args Command line arguments
   */
  public void run(String[] args) {
    Command command = Command.NONE;

    String projectPath = "";
    for (int i = 0; i < args.length; ++i) {
      String arg = args[i];
      if (arg.equals("analyze")) {
        if (command != Command.NONE) {
          System.err.println("Error: only one command can be selected!");
          System.exit(1);
        }
        command = Command.ANALYZE;
      } else if (arg.equals("runtests")) {
        if (command != Command.NONE) {
          System.err.println("Error: only one command can be selected!");
          System.exit(1);
        }
        command = Command.RUNTESTS;
      } else if (arg.equals("compile")) {
        if (command != Command.NONE) {
          System.err.println("Error: only one command can be selected!");
          System.exit(1);
        }
        command = Command.COMPILE;
      } else if (arg.equals("runall")) {
        if (command != Command.NONE) {
          System.err.println("Error: only one command can be selected!");
          System.exit(1);
        }
        // Run all tests.
        command = Command.RUNALL;
      } else if (arg.equals("-project")) {
        if (i + 1 == args.length) {
          Log.error("You must specify a project path");
          System.exit(1);
          return;
        }
        projectPath = args[++i];
      } else if (arg.equals("-help")) {
        // Print help then exit.
        printUsage();
        return;
      }
    }
    try {
      switch (command) {
        case NONE:
          printUsage();
          Log.error("You must specify a command.");
          System.exit(1);
          return;
        case COMPILE:
          setProgressListener(new ConsoleProgressListener());
          initProject(projectPath);
          int javacStatus = compileSources();
          if (javacStatus != 0) {
            System.err.println("Can not run tests; compile error exists!");
            System.exit(1);
          }
          break;
        case ANALYZE:
          setProgressListener(new ConsoleProgressListener());
          initProject(projectPath);
          parseDependencies(SaveOption.FORCE_SAVE);
          break;
        case RUNTESTS:
          setProgressListener(new ConsoleProgressListener());
          initProject(projectPath);
          parseDependencies(SaveOption.DO_NOT_SAVE);
          runTests(project.getTestRunner(),
              SaveOption.FORCE_SAVE,
              Collections.singleton(new ConsoleTestListener()));
          logTestStats();
          // Use System.exit() to force potential idling test
          // threads (e.g., JUnit thread pools).
          System.exit(0);
          break;
        case RUNALL:
          setProgressListener(new ConsoleProgressListener());
          initProject(projectPath);
          System.out.println("Running all tests");
          runAllTests(project.getTestRunner(),
              SaveOption.DO_NOT_SAVE,
              Collections.singleton(new ConsoleTestListener()));
          // Use System.exit() to force potential idling test
          // threads (e.g., JUnit thread pools).
          System.exit(0);
          break;
      }
    } catch (Throwable t) {
      System.err.println("Uncaught exception caused abnormal termination!");
      t.printStackTrace(System.err);
      System.exit(1);
    }
  }

  private void printUsage() {
    System.out.println("usage: AutoTester [analyze|runtests] [-project xyz]");
  }

  @Override
  public void dispose() {
    if (fileSystemPoller != null) {
      fileSystemPoller.interrupt();
      fileSystemPoller = null;
    }
    if (updater != null) {
      updater.interrupt();
      updater = null;
    }
  }

  /**
   * Collect test statistics for the project
   */
  private void logTestStats() {
    int numTestFiles = 0;
    int numTests = 0;
    int pending = 0;

    for (SourceFile file : reversePathMap.values()) {
      if (file.containsTest()) {
        //Log.info("Test file: " + file.id());
        numTestFiles += 1;
        Iterator<DependencyNode> iter = file.childIterator();
        while (iter.hasNext()) {
          DependencyNode testNode = iter.next();
          if (testNode.containsTest()) {
            int testCaseCount = 0;
            Iterator<DependencyNode> childIter = testNode.childIterator();
            while (childIter.hasNext()) {
              DependencyNode child = childIter.next();
              if (child.isMethod()) {
                testCaseCount += 1;
              }
            }
            if (testCaseCount == 0) {
              // We should count this as a single unit test if it lacks identified test cases.
              testCaseCount = 1;
            }
            numTests += testCaseCount;

            if (testNode.mustRunTest()) {
              pending += testCaseCount;
            }
          }
        }
      }
    }

    Log.info(String.format("Project contains %d test files and %d unit tests (%d pending)",
        numTestFiles, numTests, pending));
  }

  /**
   * Initialize the Dependency Viewer.
   *
   * @param args
   */
  private void initGUI(String[] args) {

    // Set up the filesystem poller.
    fileSystemPoller.start();

    updater = new DependencyUpdater(this);
    updater.start();

    for (String arg: args) {
      if (arg.equals("-version")) {
        // Print the current version and exit.
        System.out.println("Dependency Viewer " + JastAddJVersion.getVersion());
        System.exit(0);
      }
    }
  }

  @Override
  public void forceDependencyRefresh() {
    updater.forceRefresh();
  }
}
