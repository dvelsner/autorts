package org.jastadd.testsel;

/**
 * @author Jesper Öqvist <jesper.oqvist@cs.lth.se>
 */
public interface ClasspathListener {
	/**
	 * Notifies the listener of changes to the classpath made by the
	 * source object.
	 * @param source
	 */
	void classpathChanged(Object source);
}
