package org.jastadd.testsel;

import java.io.PrintWriter;
import java.io.StringWriter;

import org.jastadd.testsel.meta.DependencyMap;
import org.jastadd.testsel.meta.DependencyNode;
import org.jastadd.testsel.meta.TestResult;
import org.jastadd.testsel.testapi.TestListener;

public class DepmapTestListener implements TestListener {

	private final DependencyMap map;

	public DepmapTestListener(DependencyMap map) {
		this.map = map;
	}

	@Override
	public void testFinished(final String testName, TestResult result) {
		String id;
		if (testName.endsWith("()")) {
			id = "member:" + testName;
		} else {
			id = "type:" + testName;
		}
		DependencyNode test = map.get(id);
		if (test == null) {
			System.err.println("Test node not found: " + id);
			return;
		}

		switch (result) {
		case FAILED:
			test.setTestResult(TestResult.FAILED);
			break;
		case PASSED:
			if (test.getTestResult() == TestResult.UNKNOWN) {
				test.setTestResult(TestResult.PASSED);
			}
			break;
		case UNKNOWN:
			break;
		}
	}

	@Override
	public void testStarted(String testName) {
	}

	@Override
	public void testError(String testName, Throwable thrown) {
		StringWriter writer = new StringWriter();
		thrown.printStackTrace(new PrintWriter(writer));
		String message = writer.toString();
		testError(testName, message);
	}

	@Override
	public void testError(String testName, String message) {
		String id;
		if (testName.endsWith("()")) {
			id = "member:" + testName;
		} else {
			id = "type:" + testName;
		}
		DependencyNode test = map.get(id);
		if (test == null) {
			System.err.println("Test node not found: " + id);
			return;
		}
		// TODO add TestResult.ERROR
		test.setTestResult(TestResult.FAILED);
		test.addTestError(message);
	}

	@Override
	public void testRunStarting(int numTests, int numSelectedTests) {
	}

	@Override
	public void testRunCompleted() {
	}
}