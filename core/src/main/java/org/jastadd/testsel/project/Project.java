package org.jastadd.testsel.project;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Collection;
import java.util.LinkedList;
import java.util.List;
import java.util.Properties;

import org.jastadd.classpath.ClasspathEntry;
import org.jastadd.log.Log;
import org.jastadd.testsel.testapi.TestRunner.Runner;
import org.jastadd.testsel.util.ProgramProperties;
import org.jastadd.testsel.util.RelativePath;

/**
 * Dependency project parameters are stored in objects of this class.
 * @author Jesper Öqvist <jesper.oqvist@cs.lth.se>
 */
public class Project {

	/**
	 * Default project name
	 */
	public static final String DEFAULT_NAME = "Project";

	/**
	 * Dependency map file name
	 */
	public static final String MAP_FILE_NAME = "dependencies.map";

	/**
	 * Dependency project configuration file name
	 */
	public static final String CONFIG_FILE_NAME = "dependencies.cfg";

	/**
	 * The default project instance
	 */
	public static final Project DEFAULT_PROJECT = new Project();

	/**
	 * Name of the project
	 */
	public final String name;

	/**
	 * Path to the base directory of the project
	 */
	public final String baseDir;

	private final List<String> basePath;

	private final Properties config = new Properties();

	/**
	 * Creates a project with default name and base directory
	 */
	protected Project() {
		this(DEFAULT_NAME, System.getProperty("user.dir"));
	}

	/**
	 * Creates a project with the given name and default base directory
	 * @param name The project name
	 */
	public Project(String name) {
		this(name, System.getProperty("user.dir"));
	}


	/**
	 * Creates a project with the given name and base directory
	 * @param name The project name
	 * @param path The base directory
	 */
	public Project(String name, String path) {
		this.name = name;
		this.baseDir = path;
		basePath = RelativePath.buildPathList(getBaseDir());

		loadConfig();
	}

	/**
	 * Attempt to load project configuration, if available.
	 */
	private void loadConfig() {
		File configFile = getLocalFile(CONFIG_FILE_NAME);
		if (configFile.exists()) {
			FileInputStream in = null;
			try {
				in = new FileInputStream(configFile);
				config.load(in);
			} catch (FileNotFoundException e) {
			} catch (IOException e) {
			} finally {
				if (in != null) {
					try {
						in.close();
					} catch (IOException e) {
					}
				}
			}
		}
	}

	/**
	 * Get project configuration value
	 * @param key
	 * @return config value for given key, or empty string if value was unset
	 */
	public String getConfigValue(String key) {
		return config.getProperty(key, "");
	}

	/**
	 * Set project configuration value
	 * @param key
	 * @param value
	 */
	public void setConfigValue(String key, String value) {
		config.setProperty(key, value);
	}

	/**
	 * Store the project configuration
	 */
	public void saveConfiguration() {
		try {
			config.store(new FileOutputStream(getConfigFile()), "AutoTester project configuration");
		} catch (FileNotFoundException e) {
			Log.warn("Problem while saving project configuration", e);
		} catch (IOException e) {
			Log.warn("Problem while saving project configuration", e);
		}
	}

	/**
	 * Attempts to open the previously used project
	 * @return A project instance describing the previously opened project,
	 * or the default project instance if no previous project exists
	 */
	public static Project openPreviousProject() {
		String projectPath = ProgramProperties.getProperty("projectPath");
		if (projectPath != null) {
			File projectDir = new File(projectPath);
			Project project = new Project(projectDir.getName(), projectPath);
			try {
				File mapFile = project.getMapFile().getCanonicalFile();
				if (!mapFile.exists()) {
					return DEFAULT_PROJECT;
				}
			} catch (IOException e) {
				return DEFAULT_PROJECT;
			}
			return project;
		}
		return DEFAULT_PROJECT;
	}

	/**
	 * @return The map file for this project
	 */
	public File getMapFile() {
		return getLocalFile(MAP_FILE_NAME);
	}

	/**
	 * @return The map configuration file for this project
	 */
	private File getConfigFile() {
		return getLocalFile(CONFIG_FILE_NAME);
	}

	/**
	 * @return The eclipse project .classpath file
	 */
	public File getClasspathFile() {
		return getLocalFile(".classpath");
	}

	/**
	 * @return A file object representing the base directory of this project
	 */
	public File getBaseDir() {
		return new File(baseDir);
	}

	/**
	 * Find file relative to project base directory
	 * @param path
	 * @return A reference to a file in the project given a path relative to the base directory
	 */
	public File getLocalFile(String path) {
		if (!path.equals(".")) {
			return new File(baseDir, path);
		} else {
			return new File(baseDir);
		}
	}

	/**
	 * Find file relative to project base directory
	 * @param path
	 * @return The path to a file in the project given a path relative to the base directory
	 */
	public String getLocalPath(String path) {
		return new File(baseDir, path).getPath();
	}

	/**
	 * @param path
	 * @return The path for the file relative to the project's base directory
	 */
	public String getRelativePath(String path) {
		return RelativePath.getRelativePath(path, basePath);
	}

	/**
	 * @param path
	 * @return <code>true</code> if project root path is a prefix to the given path
	 */
	public boolean isPathRelativeToProject(String path) {
		return path.startsWith(baseDir);
	}

	/**
	 * Save this project as the previous opened project.
	 */
	public void setPreviousProject() {
		ProgramProperties.setProperty("projectPath", baseDir);
	}

	@Override
	public String toString() {
		return name;
	}

	/**
	 * @return the path to the project's output directory relative to the
	 * project base directory
	 */
	public String getOutputDir() {
		return ".test-bin";
	}

	/**
	 * @return <code>true</code> if a configuration file exists for this project
	 */
	public boolean hasConfigFile() {
		return getConfigFile().exists();
	}

	/**
	 * @return The classpath entries in the project configuration
	 */
	public Collection<ClasspathEntry> getClasspath() {
		Collection<ClasspathEntry> classpath = new LinkedList<ClasspathEntry>();
		String srcPath = getConfigValue("classpath.src");
		String[] sources = srcPath.split(":");
		String[] libraries = getConfigValue("classpath.lib").split(":");
		String[] containers = getConfigValue("classpath.con").split(":");
		for (String entry: sources) {
			if (!entry.isEmpty()) {
				classpath.add(new ClasspathEntry("src", entry));
			}
		}
		if (classpath.isEmpty()) {
			// scan legacy classpath property
			sources = getConfigValue("classpath").split(":");
			for (String entry: sources) {
				if (!entry.isEmpty()) {
					if (!entry.endsWith(".jar")) {
						classpath.add(new ClasspathEntry("src", entry));
					} else {
						classpath.add(new ClasspathEntry("lib", entry));
					}
				}
			}
		}
		for (String entry: libraries) {
			if (!entry.isEmpty()) {
				classpath.add(new ClasspathEntry("lib", entry));
			}
		}
		for (String entry: containers) {
			if (!entry.isEmpty()) {
				classpath.add(new ClasspathEntry("con", entry));
			}
		}
		return classpath;
	}

	/**
	 * Set the classpath for the project
	 * @param classpath
	 */
	public void setClasspath(Collection<ClasspathEntry> classpath) {
		StringBuilder srcPath = new StringBuilder();
		StringBuilder libPath = new StringBuilder();
		StringBuilder conPath = new StringBuilder();
		for (ClasspathEntry entry: classpath) {
			if (entry.isSourcePath()) {
				if (srcPath.length() > 0) {
					srcPath.append(":");
				}
				srcPath.append(entry.path);
			} else if (entry.isLibrary()) {
				if (libPath.length() > 0) {
					libPath.append(":");
				}
				libPath.append(entry.path);
			} else if (entry.isContainer()) {
				if (conPath.length() > 0) {
					conPath.append(":");
				}
				conPath.append(entry.path);
			}
		}
		setConfigValue("classpath.src", srcPath.toString());
		setConfigValue("classpath.lib", libPath.toString());
		setConfigValue("classpath.con", conPath.toString());
	}

	/**
	 * @param path
	 * @param log <code>true</code> if problems should be logged
	 * @return <code>true</code> if the specified path points to a valid
	 * project directory
	 */
	public static boolean validateProjectPath(String path, boolean log) {
		// TODO
		return true;
	}

	/**
	 * @return the path to the configured Java compiler
	 */
	public String getJavaCompiler() {
		String javaHome = config.getProperty("java.home", "");
		File javac = locateJavac(javaHome);
		if (javac != null) {
			return javac.getAbsolutePath();
		}
		javaHome = System.getProperty("java.home");
		javac = locateJavac(javaHome);
		if (javac != null) {
			return javac.getAbsolutePath();
		}
		throw new Error("failed to locate Java compiler!");
	}

	public String[] getJavacFlags() {
		String flags = config.getProperty("javac.flags", "");
		if (flags.isEmpty()) {
			return new String[0];
		}
		return flags.split("\\s+");
	}

	private static File locateJavac(String javaHome) {
		if (javaHome.isEmpty())
			return null;
		File jdkDir;
		File jreDir = new File(javaHome);
		if (!jreDir.isDirectory()) {
			return null;
		}
		if (jreDir.getName().equals("jre")) {
			jdkDir = jreDir.getParentFile();
			if (jdkDir == null) {
				return null;
			}
		} else {
			jdkDir = jreDir;
		}
		File binDir = new File(jdkDir, "bin");
		if (!binDir.isDirectory()) {
			return null;
		}
		File javac = new File(binDir, "javac");
		if (javac.canExecute()) {
			return javac;
		}
		javac = new File(binDir, "javac.exe");
		if (javac.canExecute()) {
			return javac;
		}
		return null;
	}

	public void setTestRunner(Runner runner) {
		config.setProperty("testRunner", runner.name());
	}

	public Runner getTestRunner() {
		try {
			return Runner.valueOf(config.getProperty("testRunner", ""));
		} catch (IllegalArgumentException e) {
			return Runner.JUNIT4;
		}
	}

  public String[] getSourceExcludePatterns() {
    return config.getProperty("exclude", "").split("\\s*,\\s*");
  }

	/**
	 * Retrieve the configured boot classpath
	 * @return boot classpath array
	 */
	public String[] getBootClasspath() {
		String javaHome = config.getProperty("java.home", "");
		if (!javaHome.isEmpty()) {
			File jreDir = new File(javaHome);
			if (jreDir.isDirectory()) {
				File libDir = new File(jreDir, "lib");
				List<String> paths = new LinkedList<String>();
				for (File file: libDir.listFiles()) {
					if (file.getName().toLowerCase().endsWith(".jar")) {
						paths.add(file.getAbsolutePath());
					}
				}
				if (!paths.isEmpty()) {
					String[] classpath = new String[paths.size()];
					paths.toArray(classpath);
					return classpath;
				}
			}
		}
		// default bootclasspath
		return System.getProperty("sun.boot.class.path").split(File.pathSeparator);
	}

}
