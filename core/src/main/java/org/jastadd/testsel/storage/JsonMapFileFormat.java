package org.jastadd.testsel.storage;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintStream;
import java.util.Collection;
import java.util.Iterator;
import java.util.LinkedList;

import org.jastadd.testsel.meta.CacheState;
import org.jastadd.testsel.meta.DependencyGroup;
import org.jastadd.testsel.meta.DependencyMap;
import org.jastadd.testsel.meta.DependencyNode;
import org.jastadd.testsel.meta.MemberDependency;
import org.jastadd.testsel.meta.ReflectionTypeDependency;
import org.jastadd.testsel.meta.SourceFile;
import org.jastadd.testsel.meta.SourceProblem;
import org.jastadd.testsel.meta.TestKind;
import org.jastadd.testsel.meta.TestResult;
import org.jastadd.testsel.meta.TypeDependency;
import org.jastadd.testsel.meta.UnknownSource;
import org.jastadd.testsel.meta.filter.LibraryNodeFilter;
import org.jastadd.util.PrettyPrinter;

import se.llbit.json.JsonArray;
import se.llbit.json.JsonObject;
import se.llbit.json.JsonParser;
import se.llbit.json.JsonParser.SyntaxError;
import se.llbit.json.JsonString;
import se.llbit.json.JsonValue;

public class JsonMapFileFormat implements MapFileFormat {

	static final int VERSION = 7;

	/*
	 * Version log:
	 *
	 * version 8: added testErrors arrays
	 * version 7: default package now has the ID "package:(default)"
	 * version 6: package children are now in the "children" element rather than the "files" element
	 * version 5: node IDs now include node type specifier
	 * version 4: compact dependency encoding (type:, file:, reflection:)
	 * version 3: removed type and file arrays, only package array is on top level
	 * version 2: store source problems in map
	 */

	DependencyMap map;

	@Override
	public void readMap(DependencyMap map, File mapFile) throws IOException {
		this.map = map;
		try {
			FileInputStream in = new FileInputStream(mapFile);
			JsonParser parser = new JsonParser(in);
			JsonObject json = parser.parse().object();
			fromJson(json);
			in.close();
		} catch (SyntaxError e) {
			System.err.println("JSON format error!");
		}
	}

	private void fromJson(JsonObject json) throws DependencyMapIOError {
		int version = json.get("version").intValue(0);
		if (version != VERSION) {
			System.err.println("WARNING: cannot load incompatible map file version!");
			return;
		}
		JsonArray packages = json.get("packages").array();
		for (int i = 0; i < packages.getNumElement(); ++i) {
			JsonObject pkg = packages.getElement(i).object();
			packageFromJson(pkg);
		}
	}

	private void packageFromJson(JsonObject json) throws DependencyMapIOError {
		DependencyNode node = map.lookup(json.get("id").stringValue(""));
		JsonArray children = json.get("children").array();
		for (JsonValue child: children.getElementList()) {
			node.addChild(childFromJson(child.object()));
		}
	}

	private void loadChildren(DependencyNode node, JsonObject json)
			throws DependencyMapIOError {
		JsonArray children = json.get("children").array();
		for (int i = 0; i < children.getNumElement(); ++i) {
			JsonObject child = children.getElement(i).object();
			node.addChild(childFromJson(child));
		}
	}

	private void loadDependencies(DependencyNode node, JsonObject json)
			throws DependencyMapIOError {
		JsonArray dependencies = json.get("dependencies").array();
		for (int i = 0; i < dependencies.getNumElement(); ++i) {
			String dependency = dependencies.getElement(i).stringValue("");
			node.dumbAddDependency(map.lookup(dependency));
		}

	}

	private DependencyNode childFromJson(JsonObject json)
			throws DependencyMapIOError {

		int flags;

		boolean isTest;
		boolean isUserDefined;
		boolean isSafe;

		String id = json.get("id").stringValue("");
		DependencyNode node = map.lookup(id);

		if (node instanceof UnknownSource) {
			return node;
		}

		JsonArray testErrorArray = json.get("testErrors").array();
		for (int i = 0; i < testErrorArray.getNumElement(); ++i) {
			String error = testErrorArray.getElement(i).stringValue("");
			if (!error.isEmpty()) {
				node.addTestError(error);
			}
		}


		if (node instanceof SourceFile) {
			SourceFile sourceFile = (SourceFile) node;

			flags = json.get("flags").intValue(0);
			isTest = (flags & MapFileFormat.FLAG_TEST) != 0;
			isUserDefined = (flags & MapFileFormat.FLAG_USER_TEST) != 0;
			isSafe = (flags & MapFileFormat.FLAG_SAFE) != 0;
			String state = json.get("state").stringValue("");

			Collection<SourceProblem> problems = new LinkedList<SourceProblem>();
			JsonArray problemArray = json.get("problems").array();
			for (int i = 0; i < problemArray.getNumElement(); ++i) {
				problems.add(SourceProblem.fromJson(problemArray.getElement(i).object()));
			}

			sourceFile.addSourceProblems(problems);
			sourceFile.dumbSetState(CacheState.deserialize(state));

			if (!sourceFile.isGhost()) {
				sourceFile.setIsSafe(isSafe);
				// Figure out if the source file has been modified (or removed!)
				// since the dependency map was generated
				long lastModified = json.get("modtime").longValue(0);
				sourceFile.checkTimestamp(lastModified);
				sourceFile.setTimestamp(lastModified);

				sourceFile.setTestKind(TestKind.getKind(isTest, isUserDefined));
				if (isTest) {
					sourceFile.setTestResult(TestResult.fromJson(json.get("testResult")));
				}
			}

			loadChildren(node, json);
			loadDependencies(node, json);
		}

		if (node instanceof DependencyGroup) {
			loadChildren(node, json);
		}

		if (node instanceof TypeDependency) {
			TypeDependency type = (TypeDependency) node;

			if (!(node instanceof ReflectionTypeDependency)) {
				flags = json.get("flags").intValue(0);
				isTest = (flags & MapFileFormat.FLAG_TEST) != 0;
				isUserDefined = (flags & MapFileFormat.FLAG_USER_TEST) != 0;
				isSafe = (flags & MapFileFormat.FLAG_SAFE) != 0;
				type.setTestKind(TestKind.getKind(isTest, isUserDefined));
				type.setIsSafe(isSafe);

				type.setTestResult(TestResult.fromJson(json.get("testResult")));

				loadChildren(type, json);
				loadDependencies(type, json);
			}
		}

		if (node instanceof MemberDependency) {
			MemberDependency method = (MemberDependency) node;

			flags = json.get("flags").intValue(0);
			TestResult result = TestResult.fromJson(json.get("testResult"));

			isTest = (flags & MapFileFormat.FLAG_TEST) != 0;
			isUserDefined = (flags & MapFileFormat.FLAG_USER_TEST) != 0;
			isSafe = (flags & MapFileFormat.FLAG_SAFE) != 0;
			method.setTestKind(TestKind.getKind(isTest, isUserDefined));
			method.setIsSafe(isSafe);
			method.setTestResult(result);
		}

		return node;
	}

	@Override
	public void writeMap(DependencyMap map, File mapFile) throws IOException {
		this.map = map;
		JsonObject obj = toJson();
		PrintStream out = new PrintStream(new BufferedOutputStream(new FileOutputStream(mapFile)));
		PrettyPrinter pp = new PrettyPrinter("  ", out);
		pp.print(obj);
		out.close();
	}

	private JsonObject toJson() throws DependencyMapIOError {
		JsonObject json = new JsonObject();

		json.add("version", VERSION);

		Iterator<DependencyNode> iter = map.packageIterator();
		JsonArray packages = new JsonArray();
		packages.addElement(packageToJson(map.librariesGroup()));
		while (iter.hasNext()) {
			DependencyNode pkg = iter.next();
			packages.addElement(packageToJson(pkg));
		}
		json.add("packages", packages);
		return json;
	}

	private JsonObject packageToJson(DependencyNode pkg) throws DependencyMapIOError {
		JsonObject json = pkg.toJson();
		Iterator<DependencyNode> iter = pkg.childIterator();
		JsonArray children = new JsonArray();
		while (iter.hasNext()) {
			DependencyNode child = iter.next();
			children.addElement(childToJson(child));
		}
		if (children.getNumElement() > 0) {
			json.add("children", children);
		}
		return json;
	}

	private void addChildren(JsonObject json, DependencyNode node) throws DependencyMapIOError {
		JsonArray children = new JsonArray();
		for (DependencyNode child: node) {
			children.add(childToJson(child));
		}
		if (children.getNumElement() > 0) {
			json.add("children", children);
		}
	}

	private void addDependencies(JsonObject json, DependencyNode node) throws DependencyMapIOError {
		JsonArray dependencies = new JsonArray();
		Iterator<DependencyNode> iter = node.dependencyIterator();
		while (iter.hasNext()) {
			DependencyNode dependency = iter.next();
			if (LibraryNodeFilter.INSTANCE.pass(dependency)) {
				dependencies.add(dependency.id());
			}
		}
		if (dependencies.getNumElement() > 0) {
			json.add("dependencies", dependencies);
		}
	}

	private JsonObject childToJson(DependencyNode node) throws DependencyMapIOError {

		JsonObject json = node.toJson();
		Collection<String> testErrors = node.testErrors();
		if (!testErrors.isEmpty()) {
			JsonArray testErrorArray = new JsonArray();
			for (String testError: testErrors) {
				testErrorArray.addElement(new JsonString(testError));
			}
			json.add("testErrors", testErrorArray);
		}

		if (node instanceof SourceFile) {
			SourceFile sourceFile = (SourceFile) node;
			json.add("flags", flags(node));
			json.add("state", node.getState().name());
			if (!node.isGhost()) {
				json.add("modtime", sourceFile.getLastModified());
				if (node.isTest()) {
					json.add("testResult", node.getTestResult().name());
				}
			}
			Collection<SourceProblem> problems = node.problems();
			if (!problems.isEmpty()) {
				JsonArray problemArray = new JsonArray();
				for (SourceProblem problem: problems) {
					problemArray.addElement(problem.toJson());
				}
				json.add("problems", problemArray);
			}

			addChildren(json, sourceFile);
			addDependencies(json, sourceFile);
		} else if (node instanceof MemberDependency) {

			json.add("flags", flags(node));

		} else if ((node instanceof TypeDependency) &&
				!(node instanceof ReflectionTypeDependency)) {

			json.add("flags", flags(node));
			addChildren(json, node);
			addDependencies(json, node);

		} else if (node instanceof DependencyGroup) {

			// Do not add children of second-level dependency groups
			//addChildren(json, node);

		} else {
			throw new DependencyMapIOError(
					"Unsupported child node type during map serilization: " +
							node.getClass().getSimpleName());
		}

		if (node.isTest()) {
			json.add("testResult", node.getTestResult().name());
		}
		return json;
	}

	private static int flags(DependencyNode node) {
		return (node.isSafe() ? MapFileFormat.FLAG_SAFE : 0x00)
				| (node.getTestKind().isTest ? MapFileFormat.FLAG_TEST : 0x00)
				| (node.getTestKind().isUserDefined ? MapFileFormat.FLAG_USER_TEST : 0x00);
	}

}
