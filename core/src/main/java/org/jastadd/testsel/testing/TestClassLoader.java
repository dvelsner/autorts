package org.jastadd.testsel.testing;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Collection;
import java.util.LinkedList;

import org.jastadd.log.Log;

/**
 * Loads test classes for the test cases to be tested.
 *
 * @author Jesper Öqvist <jesper.oqvist@cs.lth.se>
 */
public class TestClassLoader extends ClassLoader {

	private final Collection<String> testClasspath;

	/**
	 * Create a new test ClassLoader.
	 * @param testClasspath The classpath used to find the test classes.
	 */
	public TestClassLoader(Collection<String> testClasspath) {
		this.testClasspath = new LinkedList<String>(testClasspath);
	}

	@Override
	public Class<?> loadClass(String name) throws ClassNotFoundException {

		Class<?> testClass = findLoadedClass(name);
		if (testClass != null)
			return testClass;

		if (name.startsWith("org.jastadd"))
			return findClass(name);
		else
			return super.loadClass(name);
	}

	@Override
	protected Class<?> findClass(String name) throws ClassNotFoundException {
		try {
			Log.info("loadTestClass(String): " + name);
			String classFileName = name.replace(".", File.separator) + ".class";
			for (String classPath : testClasspath) {
				File classFile = new File(classPath + File.separator + classFileName);
				if (classFile.exists()) {
					Log.info("found test class: " + classFile.getAbsolutePath());
					FileInputStream in = new FileInputStream(classFile);
					int length = (int) classFile.length();
					byte[] data = new byte[length];
					int offset = 0;
					int len;
					while (offset < length &&
						(len = in.read(data, offset, length - offset)) != -1) {

						offset += len;
					}
					in.close();
					return defineClass(name, data, 0, offset);
				}
			}
		} catch (IOException e) {
		}
		throw new ClassNotFoundException();
	}

}
