package org.jastadd.testsel.tool;

import org.extendj.ast.BeaverParser;
import org.extendj.ast.BytecodeParser;
import org.extendj.ast.BytecodeReader;
import org.extendj.ast.ClassFolderPath;
import org.extendj.ast.ClassPath;
import org.extendj.ast.CompilationUnit;
import org.extendj.ast.Frontend;
import org.extendj.ast.JarFilePath;
import org.extendj.ast.Options;
import org.extendj.ast.Problem;
import org.extendj.ast.Program;
import org.extendj.ast.SourceFolderPath;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintStream;
import java.util.Collection;
import java.util.Iterator;
import java.util.LinkedList;

import org.jastadd.classpath.ClasspathEntry;
import org.jastadd.log.Log;
import org.jastadd.testsel.meta.DependencyMap;
import org.jastadd.testsel.meta.UnknownSource;
import org.jastadd.testsel.project.Project;
import org.jastadd.testsel.storage.JsonMapFileFormat;
import org.jastadd.testsel.storage.MapFileFormat;
import org.jastadd.testsel.storage.MapFormat;
import org.jastadd.util.RobustMap;

/**
 * Abstract superclass for dependency mapping tools.
 * Parses a program to build a dependency map.
 * Saves and loads dependency maps.
 *
 * @author Jesper Öqvist <jesper.oqvist@cs.lth.se>
 */
public abstract class DependencyTool extends DependencyMap {

	private static MapFormat format = MapFileFormat.DEFAULT_FORMAT;

	/**
	 * Parse the dependency project config file.
	 *
	 * @throws IOException
	 */
	protected void loadProject(Project project) throws IOException {
		loadClasspath(project);
	}

	private void loadClasspath(Project project) {
		classpath = project.getClasspath();
		notifyClasspathChanged(this);
	}

	/**
	 * Parse the dependency map and reload dependencies for files that have
	 * changed.
	 *
	 * @throws IOException
	 */
	protected void loadMap(File mapFile) throws IOException {
		Log.info("Loading map file " + mapFile.getPath());

		MapFileFormat mapFileFormat = getMapFormat();

		DependencyMap parsedMap = new DependencyMap();
		parsedMap.project = project;
		mapFileFormat.readMap(parsedMap, mapFile);
		transferDependencies(parsedMap);
	}

	/**
	 * Parse a program and build a dependency map over all source files.
	 * @param program
	 * @return exit code (0 indicates success)
	 */
	protected int parseProgram(Program program) {
		return parseProgram(program, System.out, System.err);
	}

	/**
	 * Parse a program and build a dependency map over all source files.
	 * @param program
	 * @return exit code (0 indicates success)
	 */
	@SuppressWarnings("unchecked")
	protected int parseProgram(Program program, PrintStream out,
			PrintStream err) {
		resetDependencyMap();

		Options options = program.options();
		if (classpath.isEmpty()) {
			if (options.hasValueForOption("-classpath")) {
				for (String cp : options.getValueForOption("-classpath").split(
						File.pathSeparator)) {
					classpath.add(new ClasspathEntry("src", cp));
				}
			} else if (options.hasValueForOption("-cp")) {
				for (String cp : options.getValueForOption("-cp").split(
						File.pathSeparator)) {
					classpath.add(new ClasspathEntry("src", cp));
				}
			}
		}

		Collection<File> sourceFiles = new LinkedList<File>();
		for (String fileName : options.files()) {
			File file = new File(fileName);
			if (file.exists()) {
				sourceFiles.add(file);
			}
		}
		if (sourceFiles.isEmpty()) {
			sourceFiles = getSourceFiles();
		}

		out.println("Parsing program source files");
		long parseStart = System.currentTimeMillis();

		int exitValue = Frontend.EXIT_SUCCESS;

		try {
			for (File file : sourceFiles) {
				String path = file.getPath();
				program.addSourceFile(path);
			}

			Iterator<CompilationUnit> iter = program.compilationUnitIterator();
			while (iter.hasNext()) {
				CompilationUnit unit = iter.next();

				Collection<Problem> errors = unit.parseErrors();
				Collection<Problem> warnings = new LinkedList<Problem>();
				if (!errors.isEmpty()) {
					// report syntax errors
					for (Problem error : errors) {
						err.println(error);
						exitValue = Frontend.EXIT_ERROR;
					}
				} else {
					//Log.debug("Adding dependencies: " + unit.pathName());
					unit.fillInDependencies(this);
				}
			}

			// Fill in library dependencies.
			Iterator<CompilationUnit> cuit = program.libraryCompilationUnitIterator();
			while (cuit.hasNext()) {
				CompilationUnit unit = cuit.next();
				if (unit != null) {
					//Log.debug("Adding dependencies: " + unit.pathName());
					unit.fillInDependencies(this);
				}
			}
		} catch (Exception e) {
			err.println("Unexpected exception while parsing program:");
			e.printStackTrace(err);
			exitValue = Frontend.EXIT_SYSTEM_ERROR;
		}

		long parseTime = System.currentTimeMillis() - parseStart;
		long parseSecs = parseTime / 1000;
		out.println(String.format("Dependency parsing took %d minutes, %d seconds, %d millis",
				parseSecs/60, parseSecs%60, parseTime%1000));

		out.flush();
		if (exitValue != Frontend.EXIT_SUCCESS) {
			err.flush();
		}

		// Remove the unknown-source node if it has no children
		if (unknownSource().numChildren() == 0) {
			unknownSource().getParent().removeChild(unknownSource());
			map.remove(UnknownSource.ID);
		}

		return exitValue;
	}

	protected synchronized void saveProject(Project project) throws IOException {
		Log.info("Saving configuration for project " + project);

		project.setClasspath(classpath);
		project.saveConfiguration();
	}

	protected synchronized void saveMap(File mapFile) throws IOException {
		Log.info("Saving map file " + mapFile.getPath());

		MapFileFormat mapFileFormat = getMapFormat();

		mapFileFormat.writeMap(this, mapFile);
		Log.info("Map saved");
	}

	private MapFileFormat getMapFormat() {
		switch (format) {
		case JSON:
			return new JsonMapFileFormat();
		default:
			return new JsonMapFileFormat();
		}
	}

	protected Program initProgram() {
		Program program = new Program();
		program.state().reset();
		program.initBytecodeReader(new BytecodeReader() {
      @Override
      public CompilationUnit read(InputStream is, String fullName, Program p)
          throws FileNotFoundException, IOException {
        return new BytecodeParser(is, fullName).parse(null, null, p);
      }
    });
		program.initJavaParser(new BeaverParser());
		program.options().initOptions();
		ClassPath classPath = program.getClassPath();
		classPath.initEmptyPaths();
		// boot classpath is redundant for simplified dependency analysis
		/*String[] bootcp = project.getBootClasspath();
		for (String cp: bootcp) {
			try {
				classPath.addClassPath(new JarFilePath(cp));
			} catch (IOException e) {
			}
		}*/
		for (ClasspathEntry cp: classpath) {
			/*if (cp.isLibrary()) {
				try {
					classPath.addClassPath(new JarFilePath(cp.getFile(project).getPath()));
				} catch (IOException e) {
					System.err.println("Could not use library on classpath: " + cp);
				}
			} else*/ if (cp.isSourcePath()) {
				//classPath.addClassPath(new ClassFolderPath(cp.getFile(project).getPath()));
				classPath.addSourcePath(new SourceFolderPath(cp.getFile(project).getPath()));
			}
		}
		return program;
	}

}
