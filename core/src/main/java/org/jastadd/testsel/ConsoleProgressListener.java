package org.jastadd.testsel;

import org.jastadd.testsel.project.Project;

/**
 * This progress listener does not print any output.
 * @author Jesper Öqvist <jesper.oqvist@cs.lth.se>
 */
public class ConsoleProgressListener implements ProgressListener {

	public static final ProgressListener INSTANCE = new ConsoleProgressListener();

	@Override
	public void setProgress(String string, int i, int j, int target) {
	}

	@Override
	public void resetProgress() {
	}

	@Override
	public void updateReflectionNodes() {
	}

	@Override
	public void projectLoaded(Project newProject) {
	}

	@Override
	public void updateTestResult(String testName) {
	}

	@Override
	public void synchronizeMapNonBlocking() {
	}

	@Override
	public void synchronizeMapBlocking() {
	}

	@Override
	public void beforeTestRun() {
	}

	@Override
	public void projectConfigMissing() {
	}

	@Override
	public void info(String message) {
		System.out.println(message);
	}

}
