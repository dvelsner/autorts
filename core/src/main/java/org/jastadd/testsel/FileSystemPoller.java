package org.jastadd.testsel;

import java.io.File;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.jastadd.classpath.ClasspathEntry;
import org.jastadd.log.Log;
import org.jastadd.testsel.meta.CacheState;
import org.jastadd.testsel.meta.DependencyNode;
import org.jastadd.testsel.meta.SourceFile;
import org.jastadd.testsel.project.Project;
import org.jastadd.testsel.util.DependencyViewerOptions;
import org.jastadd.testsel.util.RelativePath;

/**
 * Polls the filesystem for source file changes.
 *
 * @author Jesper Öqvist <jesper.oqvist@cs.lth.se>
 */
public class FileSystemPoller extends Thread {

	private final TestSelector map;

	private List<String> basePath;

	/**
	 * Create a new filesystem polling thread.
	 * @param app
	 */
	public FileSystemPoller(TestSelector app) {
		super("Filesystem Poller");
		this.map = app;
	}

	@Override
	public void run() {
		try {
			while (!isInterrupted()) {

				sleep(500);

				if (DependencyViewerOptions.getPollFileSystem())
					synchronizeWithFileSystem();
			}
		} catch (InterruptedException e) {
		}
	}

	/**
	 * Synchronize the cache status of the dependency nodes with the
	 * source files on the filesystem.
	 */
	public synchronized void synchronizeWithFileSystem() {
		// add all current non-deleted source files to the missing set
		Set<SourceFile> missing = new HashSet<SourceFile>();

		for (SourceFile node: map.sourceFiles()) {
			if (node.getState() != CacheState.REMOVED && !node.isGhost()) {
				missing.add(node);
			}
		}

		Project project = map.getProject();
		basePath = RelativePath.buildPathList(project.getBaseDir());

		// check for modified, added or deleted files on the classpath
		ClasspathEntry[] classpath = map.getClasspath();
		boolean modified = false;
		for (ClasspathEntry entry: classpath) {
			//File file = new File(project.getBaseDir(), entry);
			File file = entry.getFile(project);
			if (file.isDirectory()) {
				modified = checkModifiedFiles(file, "", missing) | modified;
			} else if (file.isFile()) {
				modified = checkModifiedFile(file, "", missing) | modified;
			}
		}

		missing.remove(map.unknownSource());

		// iterate through missing nodes
		for (SourceFile node: missing) {
			// bootclasspath dependencies are not discovered in the
			// classpath scanning so we need to check that the file is really missing
			String path = node.fullName();
			File file = new File(project.getBaseDir(), path);
			if (!file.exists()) {
				File externJar = new File(path);
				if (path.endsWith(".jar") && externJar.exists()) {
					// still exists - not missing
					continue;
				}
				Log.info("File deleted: " + path);

				node.setState(CacheState.REMOVED);
				modified = true;
			}
		}

		// if any file has been modified we should update the dependency views
		if (modified) {
			map.propagateNodeStates();
			map.synchronizeViewBlocking();
			map.setNeedsRefresh();
		}

	}

	/**
	 * Check if the files of the directory have been modified, added or removed
	 * from the dependency map.
	 * @param dir
	 * @param pkgName
	 * @param missing
	 * @return <code>true</code> if any file was modified, deleted or added
	 */
	private boolean checkModifiedFiles(File dir, String pkgName, Set<SourceFile> missing) {
		File[] files = dir.listFiles();
		boolean haveModified = false;

		// compare the set of existing dependency nodes with the files found on disk

		for (File file: files) {
			if (file.isDirectory()) {
				boolean modified = checkModifiedFiles(
						file,
						pkgName.isEmpty() ? file.getName() :
							pkgName + "." + file.getName(),
							missing);
				haveModified = modified | haveModified;
			} else if (file.getName().endsWith(".java")) {
				boolean modified = checkModifiedFile(file, pkgName, missing);
				haveModified = modified | haveModified;
			}
		}
		return haveModified;
	}

	/**
	 * Check if a single file has been modified
	 * @param file
	 * @param pkgName
	 * @param missing
	 * @return <code>true</code> if the file is modified
	 */
	private boolean checkModifiedFile(File file, String pkgName, Set<SourceFile> missing) {

		long modTime = file.lastModified();

		String path = RelativePath.getRelativePath(file.getPath(), basePath);
		String id = "file:" + path;
		SourceFile node = (SourceFile) map.get(id);
		if (node != null && !node.isRemoved() && !node.isGhost()) {
			boolean modified = node.checkTimestamp(modTime);

			// node still exists: remove from missing set
			missing.remove(node);

			// check for modification
			if (modified) {
				Log.info("File modified: " + path);
				node.setTimestamp(modTime);
				return true;
			}
		} else if (node != null && node.getState() == CacheState.GHOST) {
			Log.info("(ghost) Source file added: " + path);

			// Do not need to parse added libraries
			node.setState(CacheState.ADDED);
			return true;
		} else {
			boolean isJar = path.endsWith(".jar");
			if (!isJar) {
				Log.info("File added: " + path);
			}

			// add the source file to the dependency map
			SourceFile sourceFile = (SourceFile) map.lookup(id);
			if (!sourceFile.isRemoved()) {
				sourceFile.updateTimestamp(file);
			} else {
				sourceFile.setStateRecursive(CacheState.MODIFIED);
				return true;
			}

			// find the package
			if (isJar) {
				map.librariesGroup().addChild(sourceFile);
			} else if (!pkgName.isEmpty()) {
				// Do not need to parse added libraries
				DependencyNode srcPkg = map.lookup("package:" + pkgName);
				srcPkg.addChild(sourceFile);
			}

			sourceFile.setState(CacheState.ADDED);
			return true;
		}

		return false;
	}
}
