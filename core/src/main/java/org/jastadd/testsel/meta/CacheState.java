package org.jastadd.testsel.meta;

import java.util.HashMap;
import java.util.Map;

/**
 * Describes the cache state of a dependency node.
 *
 * @author Jesper Öqvist <jesper.oqvist@cs.lth.se>
 */
public enum CacheState {
	/**
	 * The node is a ghost file.
	 */
	GHOST,

	/**
	 * The node has not been modified since the last dependency
	 * refresh and does not depend on a modified node.
	 */
	CACHED,

	/**
	 * The node depends, directly or indirectly, on a node that
	 * has been modified since the last refresh.
	 */
	FLUSHED,

	/**
	 * The node has been modified since last refresh.
	 */
	MODIFIED,

	/**
	 * The node was added to the source tree since last refresh.
	 */
	ADDED,

	/**
	 * The node has been removed from the source tree since last refresh.
	 */
	REMOVED

	;

	public static final CacheState[] values = values();
	private static final Map<String,CacheState> map;

	static {
		map = new HashMap<String,CacheState>();
		for (CacheState value: values()) {
			map.put(value.name(), value);
		}
	}

	/**
	 * @param state
	 * @return state object represented by the string, or CACHED if unknown
	 */
	public static CacheState deserialize(String state) {
		CacheState value = map.get(state);
		return (value != null) ? value : CACHED;
	}
}
