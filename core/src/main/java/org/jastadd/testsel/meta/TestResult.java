package org.jastadd.testsel.meta;

import java.util.HashMap;
import java.util.Map;

import se.llbit.json.JsonValue;

/**
 * Unit test result
 * @author Jesper Öqvist <jesper.oqvist@cs.lth.se>
 *
 */
public enum TestResult {
	/**
	 * Unknown test result
	 */
	UNKNOWN,

	/**
	 * The test passed
	 */
	PASSED,

	/**
	 * The test failed
	 */
	FAILED;

	/**
	 * Used for efficient serialization
	 */
	public static TestResult[] values = values();

	private static final Map<String,TestResult> map;

	static {
		map = new HashMap<String,TestResult>();
		for (TestResult value: values()) {
			map.put(value.name(), value);
		}
	}

	/**
	 * @param testResult
	 * @return jsonValue
	 */
	public static TestResult fromJson(JsonValue jsonValue) {
		TestResult value = map.get(jsonValue.stringValue(""));
		return (value != null) ? value : UNKNOWN;
	}
}
