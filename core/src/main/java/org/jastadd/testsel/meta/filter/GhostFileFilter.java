package org.jastadd.testsel.meta.filter;

import org.jastadd.testsel.meta.DependencyNode;

public class GhostFileFilter implements DependencyNodeFilter {

	public static final GhostFileFilter INSTANCE = new GhostFileFilter();

	private GhostFileFilter() { }

	@Override
	public boolean pass(DependencyNode node) {
		return !node.isGhost();
	}
}
