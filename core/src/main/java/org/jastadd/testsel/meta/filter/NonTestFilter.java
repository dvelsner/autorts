package org.jastadd.testsel.meta.filter;

import org.jastadd.testsel.meta.DependencyNode;


public class NonTestFilter implements DependencyNodeFilter {
	public static final NonTestFilter INSTANCE = new NonTestFilter();

	private NonTestFilter() { }

	@Override
	public boolean pass(DependencyNode node) {
		return !node.containsTest();
	}

}
