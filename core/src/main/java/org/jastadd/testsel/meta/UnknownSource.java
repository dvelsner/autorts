package org.jastadd.testsel.meta;

import java.io.File;
import java.util.HashSet;
import java.util.Set;

/**
 * @author Jesper Öqvist <jesper.oqvist@cs.lth.se>
 * @production UnknownSource : {@link JarFileDependency};
 */
public class UnknownSource extends JarFileDependency {

	/**
	 * The qualified name of this node.
	 */
	public static final String ID = "file:unknown-source.jar";

	public UnknownSource() {
		super("unknown-source");
		dumbSetState(CacheState.CACHED);
	}

	@Override
	public synchronized Set<DependencyNode> allDependencies() {
		return new HashSet<DependencyNode>();
	}

	@Override
	public synchronized Set<DependencyNode> directDependencies() {
		return new HashSet<DependencyNode>();
	}

	@Override
	public String id() {
		return ID;
	}

	@Override
	public long getLastModified() {
		return 0;
	}

	@Override
	protected boolean childStateConnected() {
		return false;
	}

	@Override
	protected boolean parentStateConnected() {
		return false;
	}

	@Override
	public boolean hasDependencies() {
		return false;
	}

	@Override
	public boolean checkTimestamp() {
		return false;
	}

	@Override
	public void updateTimestamp(File file) {
	}

}
