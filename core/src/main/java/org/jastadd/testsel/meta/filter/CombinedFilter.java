package org.jastadd.testsel.meta.filter;

import org.jastadd.testsel.meta.DependencyNode;

public class CombinedFilter implements DependencyNodeFilter {

	private final DependencyNodeFilter first;
	private final DependencyNodeFilter second;

	public CombinedFilter(DependencyNodeFilter first,
			DependencyNodeFilter second) {
		this.first = first;
		this.second = second;
	}

	@Override
	public boolean pass(DependencyNode node) {
		return first.pass(node) && second.pass(node);
	}
}
