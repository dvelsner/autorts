package org.jastadd.testsel.meta;

/**
 * Thest definition kinds.
 * @author Jesper Öqvist <jesper.oqvist@cs.lth.se>
 */
public enum TestKind {
	/**
	 * Not automatically identified as a test,
	 * no user input.
	 */
	AUTO_NON_TEST(false, false),
	
	/**
	 * Automatically identified as a test,
	 * no user input.
	 */
	AUTO_TEST(true, false),
	
	/**
	 * User told us this was not a test.
	 */
	USER_NON_TEST(false, true),
	
	/**
	 * User told us this was a test.
	 */
	USER_TEST(true, true);
	
	/**
	 * <code>true</code> if this is an actual test kind
	 */
	public final boolean isTest;
	
	/**
	 * <code>true</code> if this is a user defined test kind
	 */
	public final boolean isUserDefined;
	
	TestKind(boolean isTest, boolean isUserDefined) {
		this.isTest = isTest;
		this.isUserDefined = isUserDefined;
	}

	/**
	 * @param isTest
	 * @param isUserDefined
	 * @return Test kind matching the two flags isTest and isUserDefined
	 */
	public static TestKind getKind(boolean isTest, boolean isUserDefined) {
		if (isTest) {
			if (isUserDefined)
				return TestKind.USER_TEST;
			else
				return TestKind.AUTO_TEST;
		} else {
			if (isUserDefined)
				return TestKind.USER_NON_TEST;
			else
				return TestKind.AUTO_NON_TEST;
		}
	}

}
