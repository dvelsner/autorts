package org.jastadd.testsel.meta;

import java.util.HashSet;
import java.util.Set;
import java.util.Stack;

/**
 * Type dependency node.
 *
 * <p>A type dependency is a dependency on a class, interface or enum type.
 *
 * @author Jesper Öqvist <jesper.oqvist@cs.lth.se>
 * @production TypeDependency : {@link DependencyNode} ::= DirectDependency:TypeDependency* {@link MemberDependency} ChildType:TypeDependency*;
 * @ast node
 */
public class TypeDependency extends DependencyNode {

	private final String id;

	/**
	 * Create a new type dependency node, using the fully qualified name.
	 * @param qualifiedName
	 */
	public TypeDependency(String qualifiedName) {
		super(qualifiedName);
		this.id = "type:" + qualifiedName;
	}

	@Override
	public String id() {
		return id;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	protected boolean childStateConnected() {
		return true;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	protected boolean parentStateConnected() {
		return true;
	}

	/**
	 * Add a direct dependency
	 * @param type The direct dependency
	 */
	public synchronized final void addDependency(TypeDependency type) {
		super.addDependency(type);
	}

	@Override
	public Set<DependencyNode> allDependencies() {
		Set<DependencyNode> depends = new HashSet<DependencyNode>();
		Stack<DependencyNode> worklist = new Stack<DependencyNode>();

		for (DependencyNode dep : directDependencies) {
			if (!dep.isReflection()) {
				depends.add(dep);
				worklist.add(dep);
			} else if (!isSafe()) {
				worklist.add(dep);
			}
		}

		for (DependencyNode child : children) {
			worklist.add(child);
		}

		while (!worklist.isEmpty()) {
			DependencyNode node = worklist.pop();
			for (DependencyNode dep : node.directDependencies()) {
				TypeDependency typeDep = dep.enclosingType();
				if (!depends.contains(typeDep)) {
					if (!dep.isReflection())
						depends.add(typeDep);
					worklist.add(dep);
				}
			}
		}

		depends.remove(this);
		for (DependencyNode child : children) {
			depends.remove(child);
		}
		return depends;
	}

	@Override
	public TypeDependency enclosingType() {
		return this;
	}

	@Override
	public Set<DependencyNode> directDependencies() {
		Set<DependencyNode> depends = new HashSet<DependencyNode>();

		for (DependencyNode dep : directDependencies) {
			if (!dep.isReflection())
				depends.add(dep);
		}

		for (DependencyNode child : children) {
			for (DependencyNode dep : child.directDependencies()) {
				if (!dep.isReflection())
					depends.add(dep.enclosingType());
			}
		}

		depends.remove(this);
		return depends;
	}

	@Override
	public boolean hasDependencies() {
		return allDependencies().size() > 0;
	}

	@Override
	public boolean isReflection() {
		return false;
	}

	@Override
	public void setTestResult(TestResult result) {
		setTestResultRecursive(result);
	}

	@Override
	public NodeKind kind() {
		return NodeKind.JAVA_TYPE;
	}
}
