package org.jastadd.testsel.meta;

/**
 * Field dependency node.
 *
 * @author Jesper Öqvist <jesper.oqvist@cs.lth.se>
 * @production FieldDependency : {@link MemberDependency};
 * @ast node
 */
public class FieldDependency extends MemberDependency {

	/**
	 * Create a new field dependency node.
	 * @param qualifiedName The fully qualified name of the field.
	 */
	public FieldDependency(String qualifiedName) {
		super(qualifiedName);
	}

	@Override
	public NodeKind kind() {
		return NodeKind.JAVA_FIELD;
	}
}
