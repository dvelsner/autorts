package org.jastadd.testsel.meta.filter;

import org.jastadd.testsel.meta.DependencyNode;

/**
 * Filters dependency nodes
 * @author Jesper Öqvist <jesper.oqvist@cs.lth.se>
 */
public interface DependencyNodeFilter {
	/**
	 * Test if the given dependency node passes the filter
	 * @param node
	 * @return <code>true</code> if the node passes the filter
	 */
	boolean pass(DependencyNode node);
}
