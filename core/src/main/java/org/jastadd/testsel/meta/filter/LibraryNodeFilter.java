package org.jastadd.testsel.meta.filter;

import org.jastadd.testsel.meta.DependencyNode;
import org.jastadd.testsel.meta.JarFileDependency;

/**
 * Filter out library nodes.
 * @author Jesper Öqvist <jesper.oqvist@cs.lth.se>
 */
public class LibraryNodeFilter implements DependencyNodeFilter {

	public static final LibraryNodeFilter INSTANCE = new LibraryNodeFilter();

	private LibraryNodeFilter() { }

	@Override
	public boolean pass(DependencyNode node) {
		return !(node.getParent() instanceof JarFileDependency);
	}
}
