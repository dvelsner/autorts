package org.jastadd.testsel.meta;

import java.io.File;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

/**
 * Jar file dependency node.
 *
 * @author Jesper Öqvist <jesper.oqvist@cs.lth.se>
 * @production JarFileDependency : {@link SourceFile};
 * @ast node
 */
public class JarFileDependency extends SourceFile {

	/**
	 * Create a new jar file dependency node.
	 * @param path The path name of the source file
	 */
	public JarFileDependency(String path) {
		super(new File(path), path);
	}

	/**
	 * Create a new jar file dependency node.
	 * @param file The source file
	 * @param path local file path
	 */
	public JarFileDependency(File file, String path) {
		super(file, path);
	}

	@Override
	public synchronized Set<DependencyNode> allDependencies() {
		Set<DependencyNode> depends = new HashSet<DependencyNode>();

		for (DependencyNode child : children) {
			for (DependencyNode dep : child.allDependencies()) {
				if (!dep.isReflection() && dep.enclosingJar() != null)
					depends.add(dep.enclosingJar());
			}
		}

		depends.remove(this);
		return depends;
	}

	@Override
	public synchronized Set<DependencyNode> directDependencies() {
		Set<DependencyNode> depends = new HashSet<DependencyNode>();

		for (DependencyNode child : children) {
			for (DependencyNode dep : child.directDependencies()) {
				if (!dep.isReflection() && dep.enclosingJar() != null)
					depends.add(dep.enclosingJar());
			}
		}

		depends.remove(this);
		return depends;
	}

	@Override
	public SourceFile enclosingFile() {
		if (parent == null)
			return null;
		return parent.enclosingFile();
	}

	@Override
	public JarFileDependency enclosingJar() {
		return this;
	}

	@Override
	public synchronized Collection<DependencyNode> getDependents() {
		Set<DependencyNode> dependents = new HashSet<DependencyNode>();
		for (DependencyNode child : children) {
			for (DependencyNode dependent : child.getDependents()) {
				JarFileDependency enclosingJar = dependent.enclosingJar();
				if (enclosingJar != null)
					dependents.add(enclosingJar);
			}
		}
		dependents.remove(this);
		return dependents;
	}

	@Override
	public boolean isLibraryNode() {
		return true;
	}

	@Override
	public NodeKind kind() {
		return NodeKind.JAR_FILE;
	}
}

