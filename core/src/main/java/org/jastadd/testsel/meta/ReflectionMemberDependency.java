package org.jastadd.testsel.meta;

import java.util.Iterator;

/**
 * Reflection dependency node.
 * A ReflectionDependency must be connected to a DependencyMap.
 * The reflection dependency uses the dependency map to calculate
 * it's dependencies.
 *
 * @author Jesper Öqvist <jesper.oqvist@cs.lth.se>
 * @production ReflectionMemberDependency : {@link MemberDependency};
 * @ast node
 */
public class ReflectionMemberDependency extends MemberDependency {

	/**
	 * The qualified name of this node.
	 */
	public static final String ID = "reflection:member";
	private final DependencyMap map;

	/**
	 * Create a new reflection member dependency node.
	 * @param map The dependency map that this reflection dependency will
	 * be a part of.
	 */
	public ReflectionMemberDependency(DependencyMap map) {
		super("reflection:member");
		this.map = map;
	}

	@Override
	public String id() {
		return ID;
	}

	@Override
	public boolean hasDependencies() {
		return map.hasTypeDependencies();
	}

	@Override
	public Iterator<DependencyNode> dependencyIterator() {
		return map.typeIterator();
	}

	@Override
	public int numDependencies() {
		return map.numTypeDependencies();
	}

	@Override
	public boolean isReflection() {
		return true;
	}

	@Override
	public NodeKind kind() {
		return NodeKind.JAVA_REFLECTION_MEMBER;
	}
}
