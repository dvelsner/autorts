package org.jastadd.testsel.meta.filter;

import org.jastadd.testsel.meta.DependencyNode;

/**
 * Filters nodes with a name that match the string, or contain
 * a child that matches the filter.
 * @author Jesper Öqvist <jesper.oqvist@cs.lth.se>
 */
public class NodeNameFilter implements DependencyNodeFilter {

	private final String namePart;

	/**
	 * @param namePart accept only nodes containing the name part
	 */
	public NodeNameFilter(String namePart) {
		this.namePart = namePart;
	}

	@Override
	public boolean pass(DependencyNode node) {
		return node.name().contains(namePart);
	}

}
