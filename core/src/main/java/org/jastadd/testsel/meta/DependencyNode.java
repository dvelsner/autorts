package org.jastadd.testsel.meta;

import org.extendj.ast.Problem;

import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Queue;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

import org.jastadd.testsel.meta.filter.DependencyNodeFilter;
import org.jastadd.testsel.util.Tuple;

import se.llbit.json.JsonObject;

/**
 * Abstract dependency node.
 *
 * <p>The dependency map is organized into packages, files, types and members.
 * The dependency classes represent the following AST:
 *
 * <pre>
 * abstract DependencyNode ::= Parent:DependencyNode;
 *
 * [PACKAGE]
 * {@link SourcePackage} : DependencyNode ::= SourceFile* SourcePackage*;
 * {@link DefaultPackage} : SourcePackage;
 *
 * [FILE]
 * {@link SourceFile} : DependencyNode ::= TypeDependency*;
 * {@link JarFileDependency} : SourceFile;
 * {@link UnknownSource} : JarFileDependency;
 *
 * [TYPE]
 * {@link TypeDependency} : DependencyNode ::= DirectDependency:TypeDependency* MemberDependency* ChildType:TypeDependency*;
 * {@link ReflectionTypeDependency} : TypeDependency;
 *
 * [MEMBER]
 * abstract {@link MemberDependency} : DependencyNode ::= DirectDependency:MemberDependency*;
 * {@link FieldDependency} : MemberDependency;
 * {@link MethodDependency} : MemberDependency;
 * {@link ReflectionMemberDependency} : MemberDependency;
 * </pre>
 *
 * <p>The dependency map is stratified into four layers: package layer,
 * file layer, type layer and member layer. Dependencies are only tracked
 * between nodes in the same layer, so a package node will never be dependent
 * on a type node and vice versa, although the lower layers dictate the dependencies
 * of nodes in the layers above. For example if a file A.java contains type A which depends
 * on type B in file B.java then A.java depends on B.java.
 *
 * <p>The member layer does currently not support dependency tracking. Dependencies
 * are only tracked from the type layer and up.!
 *
 * <p>The two reflection classes are needed to represent uses of reflection in
 * their respective layers.
 * The Member layer is only used if member-level dependency mapping
 * is used. Member-level dependency mapping is not yet implemented.
 *
 * <p>Only type dependencies and member dependencies can have direct dependencies.
 *
 * <p>Dependency nodes can be flushed - indicating that they depend, possibly
 * through several indirect dependencies, on something that has been modified.
 * All types in a source file that has been modified are currently flushed.
 * In the future we would like to only flush the types that were actually
 * modified, and simple edits that do not affect the parsed AST of the type
 * should not cause the type to be flushed or considered modified.
 *
 * <p>Test cases that are flushed need to be re-run (test selection).
 *
 * <p>The name and qualifier of a dependency node do not have to be equal.
 * The qualifier must be a unique String that can be used to unambiguously
 * identify the dependency node.
 *
 * @author Jesper Öqvist <jesper.oqvist@cs.lth.se>
 * @production abstract DependencyNode ::= Parent:DependencyNode;
 * @ast node
 */
public abstract class DependencyNode implements Comparable<DependencyNode>,
	Iterable<DependencyNode> {

	protected DependencyNode parent = null;

	/**
	 * Flag indicating whether this node is a unit test or not.
	 */
	private TestKind testKind = TestKind.AUTO_NON_TEST;

	/**
	 * The result of the unit test.
	 */
	private TestResult testResult = TestResult.UNKNOWN;

	/**
	 * The state of this node. One of CACHED, FLUSHED and MODIFIED.
	 * CACHED tests do not need to be refreshed.
	 * A FLUSHED node must have all its dependent nodes FLUSHED as well.
	 */
	protected CacheState state = CacheState.CACHED;

	/**
	 * Children. Not the same as dependencies.
	 * Only one thread may write this, but several can read.
	 */
	protected Collection<DependencyNode> children =
			Collections.newSetFromMap(new ConcurrentHashMap<DependencyNode, Boolean>());

	/**
	 * Direct dependencies of this node.
	 * Only one thread may write this, but several can read.
	 */
	protected Collection<DependencyNode> directDependencies =
			Collections.newSetFromMap(new ConcurrentHashMap<DependencyNode, Boolean>());

	/**
	 * Reverse dependencies. Used to flush dependent nodes when the current
	 * node becomes flushed.
	 * Only one thread may write this, but several can read.
	 */
	protected Collection<DependencyNode> reverseDependencies =
			Collections.newSetFromMap(new ConcurrentHashMap<DependencyNode, Boolean>());

	private boolean isSafe;

	protected Collection<SourceProblem> problems = new LinkedList<SourceProblem>();
	protected Collection<String> testErrors = new LinkedList<String>();

	private final String fullName;
	private final String name;

	/**
	 * Comparator to order dependencies by name.
	 */
	public static class DependencyComparator implements Comparator<DependencyNode> {
		@Override
		public int compare(DependencyNode arg0, DependencyNode arg1) {
			return arg0.name().compareTo(arg1.name());
		}
	}

	/**
	 * Construct a new dependency node with the given full node name
	 * and the simple name.
	 * @param fullName
	 * @param simpleName
	 */
	protected DependencyNode(String fullName, String simpleName) {
		this.fullName = fullName;
		this.name = simpleName;
	}

	/**
	 * Construct a new dependency node with the given full node name.
	 * The simple name is taken as the part of the full name following
	 * the last period.
	 * @param fullName
	 */
	protected DependencyNode(String fullName) {
		this.fullName = fullName;
		this.name = simpleName(fullName);
	}

	/**
	 * The simple name is part after the last period.
	 * @param qualifiedName
	 * @return simple name
	 */
	private static String simpleName(String qualifiedName) {
		String[] parts = qualifiedName.split("\\.");
		return parts[parts.length-1];
	}

	/**
	 * The fully qualified name of this node.
	 * @return The qualified name of this node
	 */
	abstract public String id();

	/**
	 * @return The simple name of this dependency node
	 */
	public final String name() {
		return name;
	}

	/**
	 * @return The full name of this dependency node
	 */
	public final String fullName() {
		return fullName;
	}

	/**
	 * Make this node dependent on the given node. Nodes are
	 * implicitly self-dependent, so adding self-dependencies is
	 * redundant. This method ignores attempts to add self-dependencies.
	 *
	 * @param node A new dependency for this node.
	 */
	public final synchronized void addDependency(DependencyNode node) {
		if (node != this) {// don't add self dependencies

			directDependencies.add(node);
			node.reverseDependencies.add(this);
			node.propagateStateChange();
		}
	}

	/**
	 * Add a multitude dependencies to this node.
	 * @param depends a collection of new dependencies for this type
	 */
	public synchronized final void addDependencies(Collection<? extends DependencyNode> depends) {
		for (DependencyNode dependency : depends) {
			addDependency(dependency);
		}
	}


	/**
	 * Add dependency without updating parent state.
	 * @param node
	 */
	public final synchronized void dumbAddDependency(DependencyNode node) {
		if (node != this) {// don't add self dependencies

			directDependencies.add(node);
			node.reverseDependencies.add(this);
		}
	}

	/**
	 * This attribute is overridden by reflection dependencies.
	 * Since reflection dependencies in effect have direct
	 * dependencies on all other nodes, the number of direct
	 * dependencies is not known until the entire dependency map
	 * has been parsed.
	 * @return The number of direct dependencies for this node.
	 */
	public synchronized int numDependencies() {
		return directDependencies.size();
	}

	@Override
	public String toString() {
		return name();
	}

	/**
	 * Calculate all dependencies of this node. This does not include
	 * the children of the node, but all dependencies of the children
	 * and all dependencies plus their dependencies are included.
	 * @return Set of all direct and indirect dependencies of this node
	 */
	public abstract Set<DependencyNode> allDependencies();

	/**
	 * Calculate the set of direct dependencies of this node.
	 * Which dependencies are counted as direct dependencies depends
	 * on the specific node type.
	 * @return Set of direct dependencies of this node
	 */
	public abstract Set<DependencyNode> directDependencies();


	/**
	 * Add a child node. It is not possible to make a node it's own child.
	 * However, it is possible but incorrect to create a circular child
	 * relation through a second node.
	 *
	 * @param node The new child.
	 */
	public final synchronized void addChild(DependencyNode node) {
		if (node != this) {// can't add self as a child

			node.setParent(this);
			children.add(node);

			if (!node.isGhost()) {
				if (isFlushed() && node.parentStateConnected()) {
					node.setState(state);
				} else if (node.isFlushed() && childStateConnected()) {
					setState(node.state);
				}
			}
		}
	}

	/**
	 * Set the parent and clear cached qualified name.
	 *
	 * @param newParent
	 */
	protected final void setParent(DependencyNode newParent) {

		// remove this node from it's previous parent, if it had one
		if (parent != null) {
			parent.children.remove(this);
		}

		parent = newParent;
	}

	/**
	 * @return <code>true</code> if this node is flushed by it's children
	 */
	protected abstract boolean childStateConnected();

	/**
	 * @return <code>true</code> if this node is flushed by it's parents
	 */
	protected abstract boolean parentStateConnected();

	/**
	 * @return The number of children of this node.
	 */
	public final synchronized int numChildren() {
		return children.size();
	}

	/**
	 * @return A iterator over the children of this node.
	 */
	public final Iterator<DependencyNode> childIterator() {
		return children.iterator();
	}

	/**
	 * @return an immutable copy of the child collection
	 */
	public Collection<DependencyNode> children() {
		return Collections.unmodifiableCollection(children);
	}

	/**
	 * @param filter
	 * @return collection of all children that pass the filter
	 */
	public synchronized Collection<DependencyNode> children(DependencyNodeFilter filter) {
		Collection<DependencyNode> set = new HashSet<DependencyNode>();
		Iterator<DependencyNode> iter = childIterator();
		while (iter.hasNext()) {
			DependencyNode child = iter.next();
			if (filter.pass(child)) {
				set.add(child);
			}
		}
		return set;
	}

	/**
	 * Children and their children (etc.)
	 * @param filter
	 * @return collection of all children that pass the filter
	 */
	public synchronized Collection<DependencyNode> childrenTransitive(
			DependencyNodeFilter filter) {
		Collection<DependencyNode> set = new HashSet<DependencyNode>();
		Iterator<DependencyNode> iter = childIterator();
		while (iter.hasNext()) {
			DependencyNode child = iter.next();
			if (filter.pass(child)) {
				set.add(child);
				set.addAll(child.childrenTransitive(filter));
			}
		}
		return set;
	}

	/**
	 * @return A iterator over the direct dependencies of this node.
	 */
	public Iterator<DependencyNode> dependencyIterator() {
		return directDependencies.iterator();
	}

	/**
	 * Attempt to set the cache state of this node. This will only
	 * have an effect if the new cache state represents a weaker
	 * cache state.
	 *
	 * The ADDED and REMOVED states are weaker than the MODIFIED state
	 * which is weaker than the FLUSHED state, which in turn is weaker
	 * than the CACHED state.
	 *
	 * @param newState The new state for the node
	 */
	public final synchronized void setState(CacheState newState) {

		if (state == CacheState.REMOVED && newState == CacheState.ADDED) {

			applyState(CacheState.MODIFIED);
		} else if (newState == CacheState.GHOST) {

			applyState(CacheState.GHOST);

		} else if (newState.ordinal() > state.ordinal()) {

			applyState(newState);

		}
	}

	/**
	 * Force the state change.
	 * @param newState
	 */
	public final synchronized void dumbSetState(CacheState newState) {
		state = newState;
	}

	/**
	 * Force a state change for this node and all its children.
	 * @param newState
	 */
	public final synchronized void setStateRecursive(CacheState newState) {
		this.state = newState;
		for (DependencyNode child: children) {
			child.setStateRecursive(newState);
		}
	}

	/**
	 * Apply a cache state change.
	 * @param newState The new cache state
	 */
	protected final synchronized void applyState(CacheState newState) {
		if (state == newState) {
			return;
		}
		state = newState;
		propagateStateChange();
	}

	/**
	 * @return The state of this node
	 */
	public CacheState getState() {
		return state;
	}

	/**
	 * Declare the test kind of this node
	 * @param kind The new test kind
	 */
	public final void setTestKind(TestKind kind) {
		if (!testKind.isUserDefined || kind.isUserDefined) {
			testKind = kind;
		}
	}

	/**
	 * @return The test kind of this node
	 */
	public final TestKind getTestKind() {
		return testKind;
	}

	/**
	 * @return {@code true} if this is a test
	 */
	public final boolean isTest() {
		return testKind.isTest;
	}

	/**
	 * Check whether this node has been flushed.
	 * @return {@code true} if this node is flushed
	 */
	public final boolean isFlushed() {
		return state != CacheState.CACHED;
	}

	/**
	 * @return {@code true} if this node has been modified or is a child of a modified node
	 */
	public final boolean isModified() {
		return state == CacheState.MODIFIED;
	}

	@Override
	public final int hashCode() {
		return id().hashCode();
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == this) {
			return true;
		}

		if (obj instanceof DependencyNode) {
			DependencyNode other = (DependencyNode) obj;
			return id().equals(other.id());
		}
		return false;
	}

	@Override
	public final int compareTo(DependencyNode o) {
		return id().compareTo(o.id());
	}

	/**
	 * Returns {@code true} if this node has at least one (possibly indirect)
	 * dependency.
	 * @return {@code true} if this node has at least one dependency
	 */
	public abstract boolean hasDependencies();

	/**
	 * Find the shortest path to a reflection dependency using breadth first search.
	 * @return a string representation of one of the shortest paths to a reflection dependency
	 */
	public final synchronized String bfsReflection() {
		Set<DependencyNode> done = new HashSet<DependencyNode>();
		Queue<Tuple<String,DependencyNode>> worklist = new LinkedList<Tuple<String,DependencyNode>>();

		for (DependencyNode node : directDependencies) {
			worklist.add(new Tuple<String, DependencyNode>(node.name(), node));
		}

		for (DependencyNode node : children) {
			worklist.add(new Tuple<String, DependencyNode>(node.name(), node));
		}

		while (!worklist.isEmpty()) {
			Tuple<String, DependencyNode> tuple = worklist.poll();
			done.add(tuple.thing2);

			if (tuple.thing2.isReflection())
				return tuple.thing1;

			for (DependencyNode dep : tuple.thing2.directDependencies) {
				if (!done.contains(dep))
					worklist.add(new Tuple<String, DependencyNode>(tuple.thing1 + "->" + dep.name(), dep));
			}
		}
		return "-none-";
	}

	/**
	 * @return The enclosing package node
	 */
	public SourcePackage enclosingPackage() {
		if (parent == null) {
			return null;
		} else {
			return parent.enclosingPackage();
		}
	}

	/**
	 * For a file node this method returns itself.
	 * @return The enclosing file node
	 */
	public SourceFile enclosingFile() {
		if (parent == null) {
			return null;
		} else {
			return parent.enclosingFile();
		}
	}

	/**
	 * For a jar node this method returns itself.
	 * @return The enclosing file node
	 */
	public JarFileDependency enclosingJar() {
		if (parent == null) {
			return null;
		} else {
			return parent.enclosingJar();
		}
	}

	/**
	 * For a type node this method returns itself.
	 * @return The enclosing type node
	 */
	public TypeDependency enclosingType() {
		if (parent == null) {
			return null;
		} else {
			return parent.enclosingType();
		}
	}

	/**
	 * @return <code>true</code> if this node depends directly on a
	 * reflection dependency node
	 */
	public final boolean reflectionDependent() {
		for (DependencyNode dep : directDependencies) {
			if (dep.isReflection()) {
				return true;
			}
		}
		return false;
	}

	/**
	 * @return <code>true</code> if this node is a reflection dependency node
	 */
	protected abstract boolean isReflection();

	/**
	 * @return The parent of this node
	 */
	public final DependencyNode getParent() {
		return parent;
	}

	/**
	 * Resets node flushed status, even if the node has flushed dependencies. Does
	 * not reset ghost state.
	 */
	public final void clearFlushed() {
		switch (state) {
		case GHOST:
			break;
		case CACHED:
			break;
		case ADDED:
		case FLUSHED:
		case MODIFIED:
		case REMOVED:
			state = CacheState.CACHED;
			break;
		}
	}

	/**
	 * @return <code>true</code> if this node has a child or grandchild
	 * that is a test
	 */
	public final boolean containsTest() {
		switch (testKind) {
		case AUTO_NON_TEST:
			break;
		case USER_NON_TEST:
			return false;
		case AUTO_TEST:
		case USER_TEST:
			return true;
		default:
			break;
		}

		for (DependencyNode child : children) {
			if (child.containsTest()) {
				return true;
			}
		}
		return false;
	}

	/**
	 * @return <code>true</code> if it is safe to use reflection in this node
	 */
	public boolean isSafe() {
		return isSafe;
	}

	/**
	 * Set the safe-ness of reflection in this node.
	 * @param isSafe
	 */
	public synchronized void setIsSafe(boolean isSafe) {
		this.isSafe = isSafe;
	}

	/**
	 * @return <code>true</code> if this node is cached
	 */
	public boolean isCached() {
		return state == CacheState.CACHED;
	}

	/**
	 * @return <code>true</code> if this is a ghost file
	 */
	public boolean isGhost() {
		return state == CacheState.GHOST;
	}

	/**
	 * Remove the node from this node's child list.
	 * @param node
	 */
	public final void removeChild(DependencyNode node) {
		children.remove(node);
	}

	/**
	 * @return A collection of the known dependents for this node
	 */
	public synchronized Collection<DependencyNode> getDependents() {
		return new LinkedList<DependencyNode>(reverseDependencies);
	}

	/**
	 * Set the result of this unit test
	 * @param result
	 */
	public void setTestResult(TestResult result) {
		this.testResult = result;
	}

	/**
	 * Set the result of this unit test
	 * @param result
	 */
	public void setTestResultRecursive(TestResult result) {
		this.testResult = result;
		for (DependencyNode child: children) {
			child.setTestResultRecursive(result);
		}
	}

	/**
	 * @return The result of the unit test
	 */
	public TestResult getTestResult() {
		if (isTest()) {
			return testResult;
		}

		TestResult aggregateResult = TestResult.PASSED;
		for (DependencyNode child : children) {

			if (!child.containsTest())
				continue;

			switch (child.getTestResult()) {
			case UNKNOWN:
				if (aggregateResult == TestResult.PASSED) {
					aggregateResult = TestResult.UNKNOWN;
				}
				break;
			case FAILED:
				aggregateResult = TestResult.FAILED;
				break;
			case PASSED:
				break;
			}
		}
		return aggregateResult;
	}

	/**
	 * @return <code>true</code> if this is a package node
	 */
	public boolean isPackage() {
		return false;
	}

	/**
	 * Remove this node from the dependency map.
	 * @param map The map to remove the node from
	 */
	public final void removeFromMap(DependencyMap map) {
		Collection<DependencyNode> children = new LinkedList<DependencyNode>();
		Iterator<DependencyNode> iter = childIterator();
		while (iter.hasNext()) {
			children.add(iter.next());
		}
		for (DependencyNode child: children) {
			child.removeFromMap(map);
		}
		for (DependencyNode dep : reverseDependencies) {
			dep.removeDependency(this);
		}
		if (parent != null)
			parent.removeChild(this);
		map.removeNode(this);
	}

	/**
	 * Remove a dependency from this node.
	 *
	 * @param node
	 */
	private void removeDependency(DependencyNode node) {
		directDependencies.remove(node);
	}

	/**
	 * @return <code>true</code> if this node has been removed from the source tree
	 */
	public boolean isRemoved() {
		return state == CacheState.REMOVED;
	}

	/**
	 * Removes all children and dependencies from this node
	 */
	public void clear() {
		children.clear();
		directDependencies.clear();
		reverseDependencies.clear();
	}

	/**
	 * @return <code>true</code> if this file should be parsed again
	 */
	public final boolean mustParse() {
		return  state.ordinal() >= CacheState.MODIFIED.ordinal();
	}

	/**
	 * @return <code>true</code> if this node's tests must be run
	 */
	public boolean mustRunTest() {
		return getTestResult() == TestResult.UNKNOWN;
	}

	/**
	 * @return <code>true</code> if this is a method dependency
	 */
	public boolean isMethod() {
		return false;
	}

	private int testDepCount = -1;
	/**
	 * @return The number of tests depending on this node
	 */
	public int numDependentTests() {
		if (testDepCount != -1) {
			return testDepCount;
		}
		testDepCount = 0;
		for (DependencyNode dependent: reverseDependencies) {
			if (dependent.isTest()) {
				testDepCount += 1;
			}
		}
		return testDepCount;
	}

	/**
	 * @return An iterator over the children of this dependency node
	 */
	@Override
	final public Iterator<DependencyNode> iterator() {
		return childIterator();
	}

	/**
	 * @param nodeFilter
	 * @return {@code true} if this node has at least one child matching the
	 * filter
	 */
	public boolean hasChildren(DependencyNodeFilter nodeFilter) {
		for (DependencyNode child: this) {
			if (nodeFilter.pass(child)) {
				return true;
			}
		}
		return false;
	}

	/**
	 * Filtered direct dependencies (including direct dependencies from children)
	 * @param filter
	 * @return filtered dependencies
	 */
	public Collection<DependencyNode> directDependencies(DependencyNodeFilter filter) {
		Collection<DependencyNode> nodes = directDependencies();
		Collection<DependencyNode> filtered = new LinkedList<DependencyNode>();
		for (DependencyNode node: nodes) {
			if (filter.pass(node)) {
				filtered.add(node);
			}
		}
		return filtered;
	}

	/**
	 * @param filter
	 * @return filtered dependencies
	 */
	public Collection<DependencyNode> getDependents(DependencyNodeFilter filter) {
		Collection<DependencyNode> nodes = getDependents();
		Collection<DependencyNode> filtered = new LinkedList<DependencyNode>();
		for (DependencyNode node: nodes) {
			if (filter.pass(node)) {
				filtered.add(node);
			}
		}
		return filtered;
	}

	/**
	 * Retrieve the source problems for this node.
	 * @return problem collection
	 */
	public Collection<SourceProblem> problems() {
		return problems;
	}

	/**
	 * Retrieve the test errors for this node.
	 * @return problem collection
	 */
	public Collection<String> testErrors() {
		return testErrors;
	}

	/**
	 * Reset the source problem collection.
	 */
	public void clearProblems() {
		problems.clear();
	}

	public boolean hasError() {
		return !problems.isEmpty();
	}

	public boolean hasParent() {
		return parent != null;
	}

	/**
	 * Serialize this node to a JSON object
	 * @return serialized JSON object
	 */
	public JsonObject toJson() {
		JsonObject json = new JsonObject();
		json.add("id", id());
		return json;
	}

	public void addProblems(Collection<Problem> probs) {
		for (Problem problem: probs) {
			problems.add(new SourceProblem(problem.line(), problem.column(), problem.message()));
		}
	}

	public void addSourceProblems(Collection<SourceProblem> probs) {
		problems.addAll(probs);
	}

	public boolean isLibraryNode() {
		return false;
	}

	/**
	 * Propagate the cache state to all dependents.
	 */
	public void propagateStateChange() {
		if (state.ordinal() >= CacheState.FLUSHED.ordinal()) {
			for (DependencyNode child : children) {
				if (child.parentStateConnected()) {
					child.setState(state);
				}
			}

			for (DependencyNode dependent : reverseDependencies) {
				if (!dependent.isSafe || !isReflection()) {
					dependent.setState(CacheState.FLUSHED);
				}
			}

			if (parent != null) {
				parent.childStateChanged(state);
			}
		}
	}

	protected void childStateChanged(CacheState state) {
		if (childStateConnected()) {
			setState(state);
		}
	}

	public void addTestError(String message) {
		testErrors.add(message);
	}

	public void clearTestErrorsRecursive() {
		testErrors.clear();
		for (DependencyNode child: children) {
			child.clearTestErrorsRecursive();
		}
	}

	abstract public NodeKind kind();

	public void setTestResultNonRecursive(TestResult result) {
		testResult = result;
	}
}
