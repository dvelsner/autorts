package org.jastadd.testsel.meta;

import java.util.HashSet;
import java.util.Set;
import java.util.Stack;

/**
 * Abstract member dependency node.
 *
 * @author Jesper Öqvist <jesper.oqvist@cs.lth.se>
 * @production abstract MemberDependency : {@link DependencyNode} ::= DirectDependency:{@link MemberDependency}*;
 * @ast node
 */
public abstract class MemberDependency extends DependencyNode {

	private final String id;

	public MemberDependency(String qualifiedName) {
		super(qualifiedName);
		this.id = "member:" + qualifiedName;
	}

	@Override
	public String id() {
		return id;
	}

	@Override
	public Set<DependencyNode> allDependencies() {
		Set<DependencyNode> depends = new HashSet<DependencyNode>();
		Stack<DependencyNode> worklist = new Stack<DependencyNode>();

		for (DependencyNode dep : directDependencies) {
			if (!dep.isReflection()) {
				depends.add(dep);
				worklist.add(dep);
			} else if (!isSafe()) {
				worklist.add(dep);
			}
		}

		for (DependencyNode child : children) {
			worklist.add(child);
		}

		while (!worklist.isEmpty()) {
			DependencyNode node = worklist.pop();
			for (DependencyNode dep : node.directDependencies()) {
				if (!depends.contains(dep)) {
					if (!dep.isReflection())
						depends.add(dep);
					worklist.push(dep);
				}
			}
		}

		depends.remove(this);
		return depends;
	}

	@Override
	public Set<DependencyNode> directDependencies() {
		return new HashSet<DependencyNode>(directDependencies);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	protected boolean childStateConnected() {
		return true;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	protected boolean parentStateConnected() {
		return true;
	}

	/**
	 * Add a direct dependency.
	 * @param member The member that this node depends on.
	 */
	public synchronized final void addDependency(MemberDependency member) {
		super.addDependency(member);
	}

	@Override
	public synchronized boolean hasDependencies() {
		if (!directDependencies.isEmpty())
			return true;
		for (DependencyNode child : children) {
			if (child.hasDependencies())
				return true;
		}
		return false;
	}

	@Override
	public boolean isReflection() {
		return false;
	}
}
