package org.jastadd.testsel.meta.filter;

import org.jastadd.testsel.meta.DependencyNode;
import org.jastadd.testsel.meta.SourcePackage;
import org.jastadd.testsel.meta.UnknownSource;

public class ProgramTreeViewFilter implements DependencyNodeFilter {

	private final DependencyNodeFilter nameFilter;
	private final DependencyNodeFilter packageNameFilter;

	public ProgramTreeViewFilter(String namePart) {
		if (namePart.isEmpty()) {
			nameFilter = PassAllFilter.INSTANCE;
			packageNameFilter = FailAllFilter.INSTANCE;
		} else {
			nameFilter = packageNameFilter = new NameFilter(namePart);
		}
	}

	@Override
	public boolean pass(DependencyNode node) {
		if (node instanceof SourcePackage) {
			if (packageNameFilter.pass(node)) {
				return true;
			}
			for (DependencyNode child: node) {
				if (pass(child)) {
					return true;
				}
			}
			return false;
		} else {
			return !(node instanceof UnknownSource) &&
				NonTestFilter.INSTANCE.pass(node) &&
				nameFilter.pass(node);
		}
	}

}
