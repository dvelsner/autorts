package org.jastadd.testsel.meta;

/**
 * Default package dependency node.
 *
 * <p>There should only exist one default package node per dependency map.
 *
 * @author Jesper Öqvist <jesper.oqvist@cs.lth.se>
 * @production DefaultPackage : {@link SourcePackage};
 * @ast node
 */
public class DefaultPackage extends SourcePackage {

	/**
	 * The qualified name of the default package.
	 */
	public static final String ID = "package:(default)";

	/**
	 * Create a new default package node. There should only be
	 * one per dependency map.
	 */
	public DefaultPackage() {
		super("(default)");
	}

	@Override
	public String id() {
		return ID;
	}
}
