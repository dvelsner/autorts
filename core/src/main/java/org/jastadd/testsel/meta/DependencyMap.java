package org.jastadd.testsel.meta;

import java.io.File;
import java.io.FileFilter;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Map;
import java.util.Queue;
import java.util.regex.Pattern;

import org.jastadd.classpath.ClasspathEntry;
import org.jastadd.testsel.ClasspathListener;
import org.jastadd.testsel.meta.filter.DependencyNodeFilter;
import org.jastadd.testsel.project.Project;

/**
 * Class to track all dependency nodes in a program.
 *
 * <p>The dependency map is a representation of the dependencies in a Java
 * program. Dependencies are tracked between packages, files, types and
 * (in the future) members.
 *
 * <p>Within one dependency map each dependency node is unique.
 * Nodes can thus be compared using reference equality to find identical
 * dependency nodes.
 *
 * @author Jesper Öqvist <jesper.oqvist@cs.lth.se>
 */
public class DependencyMap {

	/**
	 * The main map used to store all dependency nodes.
	 * The qualified names of dependency nodes are used as keys in this map.
	 */
	protected Map<String, DependencyNode> map;

	/**
	 * Maps file names to source file dependency nodes.
	 */
	protected Map<String, SourceFile> reversePathMap;

	protected Collection<TypeDependency> types;

	protected Collection<ClasspathEntry> classpath;

	int typeCounter;

	private final Collection<ClasspathListener> classpathListeners =
			new ArrayList<ClasspathListener>();

	/**
	 * The current dependency project
	 */
	public Project project = Project.DEFAULT_PROJECT;

	/**
	 * Create a new, empty, dependency map.
	 */
	public DependencyMap() {
		resetDependencyMap();
	}

	private static final FileFilter JAVA_FILTER = new FileFilter() {
		@Override
		public boolean accept(File file) {
			return file.isDirectory() || file.getName().endsWith(".java");
		}
	};

  public final synchronized FileFilter sourceFileFilter() {
    FileFilter filter = JAVA_FILTER;
    for (String exclude : project.getSourceExcludePatterns()) {
      final Pattern excludePattern = Pattern.compile(exclude);
      final FileFilter firstFilter = filter;
      filter = new FileFilter() {
        @Override
        public boolean accept(File file) {
          return firstFilter.accept(file) && !excludePattern.matcher(file.getPath()).matches();
        }
      };
    }
    return filter;
  }

	/**
	 * @return A collection containing all source files found on
	 * the current classpath
	 */
	public final synchronized Collection<File> getSourceFiles() {
		Collection<File> sourceFiles = new LinkedList<File>();
		Queue<File> worklist = new LinkedList<File>();
		for (ClasspathEntry entry : classpath) {
			if (entry.isSourcePath()) {
				worklist.add(entry.getFile(project));
			}
		}
    FileFilter sourceFileFilter = sourceFileFilter();
		while (!worklist.isEmpty()) {
			File file = worklist.poll();
			if (file.isDirectory()) {
				for (File child : file.listFiles(sourceFileFilter)) {
					worklist.add(child);
        }
			} else if (file.isFile()) {
				sourceFiles.add(file);
			}
		}
		return sourceFiles;
	}

	/**
	 * Each dependency map may currently only contain one reflection
	 * type dependency node.
	 * @return The reflection type dependency node
	 */
	public synchronized ReflectionTypeDependency reflectionType() {
		ReflectionTypeDependency reflection =
				(ReflectionTypeDependency) map.get(ReflectionTypeDependency.ID);
		if (reflection == null) {
			reflection = new ReflectionTypeDependency(this);
			map.put(reflection.id(), reflection);
		}
		return reflection;
	}

	/**
	 * Each dependency map may currently only contain one reflection
	 * member dependency node.
	 * @return The reflection type dependency node
	 */
	public synchronized ReflectionMemberDependency reflectionMember() {
		ReflectionMemberDependency reflection =
				(ReflectionMemberDependency) map.get(ReflectionMemberDependency.ID);
		if (reflection == null) {
			reflection = new ReflectionMemberDependency(this);
			map.put(reflection.id(), reflection);
		}
		return reflection;
	}

	/**
	 * @return The group containing all libraries.
	 */
	public synchronized DependencyGroup librariesGroup() {
		DependencyGroup group =
				(DependencyGroup) map.get(DependencyGroup.LIBRARIES_ID);
		if (group == null) {
			group = new DependencyGroup(DependencyGroup.LIBRARIES_GROUP);
			map.put(group.id(), group);
		}
		return group;
	}

	/**
	 * @return The group for JRE libraries
	 */
	public synchronized DependencyGroup jreGroup() {
		DependencyGroup group = (DependencyGroup) map.get(DependencyGroup.JRE_ID);
		if (group == null) {
			group = new DependencyGroup(DependencyGroup.JRE_GROUP);
			librariesGroup().addChild(group);
			map.put(group.id(), group);
		}
		return group;
	}

	/**
	 * Each dependency map may only contain one default package
	 * dependency node.
	 * @return The default package dependency node
	 */
	public synchronized DefaultPackage defaultPackage() {
		DefaultPackage pkg =
				(DefaultPackage) map.get(DefaultPackage.ID);
		if (pkg == null) {
			pkg = new DefaultPackage();
			map.put(pkg.id(), pkg);
		}
		return pkg;
	}

	/**
	 * Lookup the given type name.
	 *
	 * @param qualifiedName The fully qualified type name (including package name).
	 * @return The dependency node corresponding to the qualified type name
	 */
	private synchronized TypeDependency lookupType(String id) {
		TypeDependency type = (TypeDependency) map.get(id);
		if (type == null) {
			String qualifiedName = id.substring(5);
			type = new TypeDependency(qualifiedName);
			unknownSource().addChild(type);
			map.put(id, type);
			types.add(type);
			typeCounter += 1;
		}
		return type;
	}

	/**
	 * Lookup a method.
	 *
	 * @param signature the full method signature, including fully qualified
	 * name ({@code A.b.m(X,Y)})
	 * @return The method dependency node.
	 */
	private synchronized MethodDependency lookupMethod(String id) {
		MethodDependency method = (MethodDependency) map.get(id);
		if (method == null) {
			String signature = id.substring(7);
			method = new MethodDependency(signature);
			map.put(id, method);
		}
		return method;
	}

	private synchronized FieldDependency lookupField(String id) {
		FieldDependency field = (FieldDependency) map.get(id);
		if (field == null) {
			String name = id.substring(7);
			field = new FieldDependency(name);
			map.put(id, field);
		}
		return field;
	}

	/**
	 * @return The unknown-source file node
	 */
	public UnknownSource unknownSource() {
		UnknownSource unknown = (UnknownSource) map.get(UnknownSource.ID);
		if (unknown == null) {
			unknown = new UnknownSource();
			librariesGroup().addChild(unknown);
			map.put(UnknownSource.ID, unknown);
		}
		return unknown;
	}

	/**
	 * Find the file dependency node corresponding to a given source
	 * file's path name.
	 * @param id The ID of the source file
	 * @return The source file corresponding to the path and package pair
	 */
	private synchronized SourceFile lookupSourceFile(String id) {
		SourceFile sourceFile = (SourceFile) map.get(id);
		if (sourceFile == null) {
			String path = id.substring(5);
			if (path.endsWith(".jar")) {
				File localFile = project.getLocalFile(path);
				if (localFile.exists()) {
					// Local file exists in project.
					sourceFile = new JarFileDependency(localFile, path);
				} else {
					sourceFile = new JarFileDependency(new File(path).getAbsolutePath());
				}
				librariesGroup().addChild(sourceFile);
				return sourceFile;
			} else {
				sourceFile = new SourceFile(project.getLocalFile(path), path);
				defaultPackage().addChild(sourceFile);
			}
			map.put(id, sourceFile);
			reversePathMap.put(path, sourceFile);
		}
		return sourceFile;
	}

	/**
	 * Find the package dependency node corresponding to a given package
	 * name.
	 * @param id The name of the package
	 * @return The dependency node corresponding to the package name
	 */
	private synchronized SourcePackage lookupPackage(String id) {
		if (id.equals(DefaultPackage.ID)) {
			return defaultPackage();
		}

		SourcePackage sourcePackage = (SourcePackage) map.get(id);
		if (sourcePackage == null) {
			String pkgName = id.substring(8);
			if (pkgName.isEmpty()) {
				return defaultPackage();
			}
			sourcePackage = new SourcePackage(pkgName);
			map.put(id, sourcePackage);
		}
		return sourcePackage;
	}

	private synchronized DependencyGroup lookupGroup(String id) {
		if (id.equals(DependencyGroup.LIBRARIES_ID)) {
			return librariesGroup();
		}

		DependencyGroup group = (DependencyGroup) map.get(id);
		if (group == null) {
			String groupName = id.substring(6);
			group = new DependencyGroup(groupName);
			map.put(id, group);
		}
		return group;
	}

	/**
	 * @return An iterator that iterates over all type
	 * dependency nodes in this map
	 */
	public synchronized Iterator<DependencyNode> typeIterator() {
		// TODO make copy?
		Collection<DependencyNode> copy =
				new ArrayList<DependencyNode>(types);
		return copy.iterator();
	}

	/**
	 * @return all source packages in the dependency map
	 */
	public synchronized Collection<DependencyNode> packages() {
		Collection<DependencyNode> packages =
				new ArrayList<DependencyNode>();
		for (DependencyNode node : map.values()) {
			if (node instanceof SourcePackage) {
				packages.add(node);
			}
		}
		return packages;
	}

	/**
	 * @param filter
	 * @return all nodes matching the filter
	 */
	public synchronized Collection<DependencyNode> nodes(DependencyNodeFilter filter) {
		Collection<DependencyNode> nodes =
				new ArrayList<DependencyNode>();
		for (DependencyNode node : map.values()) {
			if (filter.pass(node)) {
				nodes.add(node);
			}
		}
		return nodes;
	}

	/**
	 * @return a copy of the collection of nodes
	 */
	public synchronized Collection<DependencyNode> nodes() {
		return new ArrayList<DependencyNode>(map.values());
	}

	/**
	 * @return An iterator that iterates over all package nodes in this map
	 */
	public synchronized Iterator<DependencyNode> packageIterator() {
		return packages().iterator();
	}

	/**
	 * @return {@code true} if there is at least one type dependency node
	 * in this dependency map
	 */
	public synchronized boolean hasTypeDependencies() {
		return typeCounter > 0;
	}

	/**
	 * @return The number of type dependency nodes in this dependency
	 * map
	 */
	public synchronized int numTypeDependencies() {
		return typeCounter;
	}

	/**
	 * Flush the entire dependency map, clearing it's data structures.
	 */
	public synchronized void resetDependencyMap() {
		map = new HashMap<String, DependencyNode>();
		reversePathMap = new HashMap<String, SourceFile>();
		types = new ArrayList<TypeDependency>();
		classpath = new ArrayList<ClasspathEntry>();
		typeCounter = 0;
		unknownSource(); // Place unknown source node in the map.
	}

	/**
	 * @return A collection of the source files in this map
	 */
	public synchronized Collection<SourceFile> sourceFiles() {
		return new LinkedList<SourceFile>(reversePathMap.values());
	}

	/**
	 * Find dependency nodes that depend on reflection.
	 * @return Set of nodes that depend on reflection
	 */
	public synchronized Collection<DependencyNode> reflectionDependentNodes() {
		Collection<DependencyNode> reflectionDependent = new LinkedList<DependencyNode>();
		for (DependencyNode node : map.values()) {
			if (node.reflectionDependent()) {
				reflectionDependent.add(node);
			}
		}
		return reflectionDependent;
	}

	/**
	 * @return The classpath for this dependency map
	 */
	public synchronized ClasspathEntry[] getClasspath() {
		ClasspathEntry[] classpathArray = new ClasspathEntry[classpath.size()];
		Iterator<ClasspathEntry> iter = classpath.iterator();
		for (int i = 0; iter.hasNext(); ++i) {
			classpathArray[i] = iter.next();
		}
		return classpathArray;
	}

	/**
	 * @return The classpath for this dependency map
	 */
	public synchronized String getClasspathString() {
		StringBuilder cp = new StringBuilder();
		ClasspathEntry[] classpathArray = getClasspath();
		boolean first = true;
		for (ClasspathEntry entry: classpathArray) {
			if (!first) {
				cp.append(":");
			}
			first = false;
			cp.append(entry.path);
		}
		return cp.toString();
	}

	/**
	 * Add an entry to the classpath for this dependency map
	 * @param entry
	 */
	public synchronized void addClasspathEntry(ClasspathEntry entry) {
		classpath.add(entry);
		notifyClasspathChanged(this);
	}

	/**
	 * Remove an entry from the classpath
	 * @param entry
	 */
	public synchronized void removeClasspathEntry(ClasspathEntry entry) {
		classpath.remove(entry);
		notifyClasspathChanged(this);
	}

	/**
	 * @return The number of source files in this dependency map
	 */
	public int numSourceFiles() {
		return reversePathMap.size();
	}

	/**
	 * Transfer the dependencies of another dependency map into this one.
	 * This method clones the references from the other map, then resets
	 * the other map.
	 * @param other The dependency map to transfer dependencies from
	 */
	public void transferDependencies(DependencyMap other) {
		classpath = other.classpath;
		map = other.map;
		reversePathMap = other.reversePathMap;
		types = other.types;
		typeCounter = other.typeCounter;

		other.resetDependencyMap();
		notifyClasspathChanged(this);
	}

	/**
	 * Remove a dependency node from this map.
	 * @param node
	 */
	public void removeNode(DependencyNode node) {
		if (node instanceof SourceFile) {
			reversePathMap.remove(node.id());
		}
		if (node instanceof TypeDependency && !(node instanceof ReflectionTypeDependency)) {
			typeCounter -= 1;
			types.remove(node);
		}
		map.remove(node.id());
	}

	/**
	 * @param qualifiedName
	 * @return <code>true</code> if the map contains a node with the qualifiedName
	 */
	public boolean containsNode(String qualifiedName) {
		return map.containsKey(qualifiedName);
	}

	/**
	 * @param qualifiedName
	 * @return The dependency node corresponding to the given qualified name,
	 * or <code>null</code> if no such node exists.
	 */
	public DependencyNode get(String qualifiedName) {
		return map.get(qualifiedName);
	}

	/**
	 * Add a classpath listener
	 * @param listener
	 */
	public void addClasspathListener(ClasspathListener listener) {
		classpathListeners.add(listener);
	}

	/**
	 * Remove a classpath listener
	 * @param listener
	 */
	public void removeClasspathListener(ClasspathListener listener) {
		classpathListeners.remove(listener);
	}

	/**
	 * Clear the classpath.
	 */
	public synchronized void clearClassPath() {
		classpath.clear();
		notifyClasspathChanged(this);
	}

	/**
	 * Notify classpath listeners that the classpath has changed.
	 * @param source The source of the event
	 */
	protected void notifyClasspathChanged(final Object source) {
		for (final ClasspathListener listener: classpathListeners) {
			listener.classpathChanged(source);
		}
	}

	/**
	 * @return Output directory URL
	 */
	public URL getOutputDir() {
		try {
			return createOutputDir().toURI().toURL();
		} catch (MalformedURLException e) {
			return null;
		}
	}

	/**
	 * @return A collection of the jar libraries referenced from the classpath
	 */
	public synchronized Collection<File> getJarLibraries() {
		Collection<File> jarFiles = new LinkedList<File>();
		Iterator<ClasspathEntry> iter = classpath.iterator();
		while (iter.hasNext()) {
			ClasspathEntry entry = iter.next();
			if (entry.isLibrary()) {
				jarFiles.add(entry.getFile(project));
			} else if (entry.isContainer()) {
				jarFiles.addAll(entry.getContainerFiles());
			}
		}
		return jarFiles;
	}

	/**
	 * Flush the dependencies and source tree data.
	 * Flushes everything but the classpath.
	 */
	public synchronized void resetDependencies() {
		map = new HashMap<String, DependencyNode>();
		reversePathMap = new HashMap<String, SourceFile>();
		types = new ArrayList<TypeDependency>();
		typeCounter = 0;
	}

	/**
	 * Create temporary directory to compile the system to.
	 * @return The temporary directory file handle
	 */
	protected File createOutputDir() {
		File dir = project.getLocalFile(project.getOutputDir());
		if (!dir.isDirectory()) {
			dir.mkdir();
		}
		return dir;
	}

	/**
	 * Recursively remove files from directory and child directories
	 * @param file
	 */
	protected static void cleanDirectory(File file) {
		if (file.isDirectory()) {
			for (File child: file.listFiles()) {
				if (child.isFile()) {
					child.delete();
				} else {
					cleanDirectory(child);
				}
			}
		}
	}

	/**
	 * Lookup a dependency node by ID.
	 * @param id
	 * @return dependency node for the ID
	 */
	public DependencyNode lookup(String id) {
		DependencyNode node = map.get(id);
		if (node != null) {
			return node;
		} else if (id.startsWith("type:")) {
			return lookupType(id);
		} else if (id.startsWith("file:")) {
			return lookupSourceFile(id);
		} else if (id.startsWith("package:")) {
			return lookupPackage(id);
		} else if (id.startsWith("member:")) {
			// TODO fields!
			if (id.endsWith("()")) {
				return lookupMethod(id);
			} else {
				return lookupField(id);
			}
		} else if (id.startsWith("group:")) {
			return lookupGroup(id);
		} else if (id.equals("reflection:type")) {
			return reflectionType();
		} else if (id.equals("reflection:member")) {
			return reflectionMember();
		} else {
			throw new Error("Unrecognized node type: " + id);
		}
	}

	/**
	 * @return {@code true} if any source file in the tree has a compile problem
	 */
	public synchronized boolean hasCompileProblem() {
		for (SourceFile file : reversePathMap.values()) {
			if (file.hasError()) {
        System.err.println("Error(s) in file " + file.id());
        for (SourceProblem problem : file.problems()) {
          System.err.println(problem);
        }
				return true;
			}
		}
		return false;
	}
}
