package org.jastadd.testsel.testapi;

import java.util.Collection;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import org.jastadd.testsel.meta.DependencyMap;
import org.jastadd.testsel.meta.DependencyNode;
import org.jastadd.testsel.meta.TestResult;

/**
 * Base class for mock test runners.
 * @author Jesper Öqvist <jesper.oqvist@cs.lth.se>
 */
public abstract class MockTestRunner implements TestRunner {

	private final List<TestListener> listeners = new LinkedList<TestListener>();

	public MockTestRunner() {
	}

	@Override
	public final boolean runTests(Collection<DependencyNode> tests,
			DependencyMap map, int numTests) {

		for (DependencyNode testNode : tests) {
			Iterator<DependencyNode> iter = testNode.childIterator();
			while (iter.hasNext()) {
				String testName = iter.next().fullName();
				testFinished(testName, getTestResult(testName));
			}
		}
		return true;
	}

	protected abstract TestResult getTestResult(String testName);

	private void testFinished(String testName, TestResult result) {
		for (TestListener listener: listeners) {
			listener.testFinished(testName, result);
		}
	}

	@Override
	public final void addListener(TestListener listener) {
		listeners.add(listener);
	}

	@Override
	public final void removeListener(TestListener listener) {
		listeners.remove(listener);
	}

}
