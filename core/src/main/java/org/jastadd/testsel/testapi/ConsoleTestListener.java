package org.jastadd.testsel.testapi;

import org.jastadd.testsel.meta.TestResult;

public class ConsoleTestListener implements TestListener {
  int numTests = 0;
  int numErrors = 0;
  private long start;

  @Override
  public void testStarted(String testName) {
  }

  @Override
  public void testError(String testName, String message) {
    numErrors += 1;
    System.out.println("testError: " + testName);
  }

  @Override
  public void testFinished(String testName, TestResult result) {
  }

  @Override
  public void testError(String testName, Throwable thrown) {
    testError(testName, thrown.getMessage());
  }

  @Override
  public void testRunStarting(int numTests, int numSelectedTests) {
    this.numTests = numSelectedTests;
    this.numErrors = 0;
    System.out.println("Running tests: " + numTests + " " + numSelectedTests);
    this.start = System.currentTimeMillis();
  }

  @Override
  public void testRunCompleted() {
    long time = System.currentTimeMillis() - start;
    System.out.println("Test run completed (took " + (time / 1000.0) + "s)");
  }
}
