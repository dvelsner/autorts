package org.jastadd.testsel.testapi;

import org.jastadd.testsel.meta.TestResult;

/**
 * Interface for test listeners
 * @author Jesper Öqvist <jesper.oqvist@cs.lth.se>
 */
public interface TestListener {

	/**
	 * Signals that a test has finished and the result is available.
	 * @param testName Qualified name of the test's dependency node
	 * @param result Test result
	 */
	void testFinished(String testName, TestResult result);

	/**
	 * Inform the listener that a test has started.
	 * @param testName Qualified name of the started test
	 */
	void testStarted(String testName);

	/**
	 * Inform the listener of a test error
	 * @param testName Qualified name of the test
	 * @param message The error message
	 */
	void testError(String testName, String message);

	/**
	 * Inform the listener of a test error
	 * @param testName Qualified name of the test
	 * @param thrown The thrown exception
	 */
	void testError(String testName, Throwable thrown);

	/**
	 * Test run is starting
	 * @param numTests
	 * @param numSelectedTests
	 */
	void testRunStarting(int numTests, int numSelectedTests);

	/**
	 * Called when the current test run has completed
	 */
	void testRunCompleted();

}
