package org.jastadd.testsel.testapi;

import org.jastadd.testsel.testapi.TestRunner.Runner;

/**
 * Creates test runners
 * @author Jesper Öqvist <jesper.oqvist@cs.lth.se>
 */
public class TestRunnerFactory {
	/**
	 * @param runner
	 * @return Test runner
	 */
	public static final TestRunner get(Runner runner) {
		switch (runner) {
		case FAILALL:
			return new FailAllTestRunner();
		case JUNIT3:
			return new JUnit3TestRunner();
		case JUNIT4:
			return new JUnit4TestRunner();
		case PASSALL:
			return new PassAllTestRunner();
		case RANDOM:
			return new PassRandomTestRunner();
		default:
			throw new Error("Unknown test runner: " + runner);
		}
	}
}
