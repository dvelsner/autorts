package org.jastadd.testsel.testapi;

import java.util.Collection;

/**
 * Interface for test runners that are able to run a collection of tests
 * represented by dependency nodes, and return the test results.
 * @author Jesper Öqvist <jesper.oqvist@cs.lth.se>
 */
public interface ExternalTestRunner {
	
	/**
	 * @param tests
	 * @param map 
	 * @param numTests Number of selected tests (not necessarily same as number of selected test nodes)
	 * @return <code>true</code> if all tests passed, false otherwise
	 */
	boolean runTests(Collection<String> tests);

	/**
	 * Add a test listener
	 * @param listener
	 */
	void addListener(TestListener listener);
	
	/**
	 * Remove a test listener
	 * @param listener
	 */
	void removeListener(TestListener listener);
}
