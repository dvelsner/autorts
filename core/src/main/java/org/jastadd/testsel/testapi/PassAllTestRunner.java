package org.jastadd.testsel.testapi;

import org.jastadd.testsel.meta.TestResult;

/**
 * A test runner that just passes all tests.
 * @author Jesper Öqvist <jesper.oqvist@cs.lth.se>
 */
public class PassAllTestRunner extends MockTestRunner {

	@Override
	protected TestResult getTestResult(String testName) {
		return TestResult.PASSED;
	}
}
