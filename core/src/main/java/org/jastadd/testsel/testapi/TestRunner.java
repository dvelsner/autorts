package org.jastadd.testsel.testapi;

import java.util.Collection;

import org.jastadd.testsel.meta.DependencyMap;
import org.jastadd.testsel.meta.DependencyNode;

/**
 * Interface for test runners that are able to run a collection of tests
 * represented by dependency nodes, and return the test results.
 * @author Jesper Öqvist <jesper.oqvist@cs.lth.se>
 */
public interface TestRunner {

	public enum Runner {
		JUNIT3("JUnit 3"),
		JUNIT4("JUnit 4"),
		PASSALL("Pass All"),
		FAILALL("Fail All"),
		RANDOM("Random"),
		;

		private final String runnerName;

		Runner(String runnerName) {
			this.runnerName = runnerName;
		}

		@Override
		public String toString() {
			return runnerName;
		}
	}

	/**
	 * @param tests
	 * @param map
	 * @param numTests Number of selected tests (not necessarily same as number of selected test nodes)
	 * @return <code>true</code> if all tests passed, false otherwise
	 */
	boolean runTests(Collection<DependencyNode> tests, DependencyMap map, int numTests);

	/**
	 * Add a test listener
	 * @param listener
	 */
	void addListener(TestListener listener);

	/**
	 * Remove a test listener
	 * @param listener
	 */
	void removeListener(TestListener listener);

}
