package org.jastadd.testsel.testapi;

import org.jastadd.testsel.meta.TestResult;

/**
 * A test runner that just fails all tests.
 * @author Jesper Öqvist <jesper.oqvist@cs.lth.se>
 */
public class FailAllTestRunner extends MockTestRunner {
	@Override
	protected TestResult getTestResult(String testName) {
		return TestResult.FAILED;
	}
}
